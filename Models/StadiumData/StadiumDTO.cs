﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Models.StadiumData
{
    public class StadiumDTO
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please enter stadium name")]
        [RegularExpression(@"^[a-zA-Z0-9''-'\s]{2,30}$", ErrorMessage = "Only letters and numbers in length of 2-30 are allowed.")]
        public string Name { get; set; }
        [Required]
        public string Address { get; set; }
    }
}
