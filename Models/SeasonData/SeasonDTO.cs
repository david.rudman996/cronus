﻿using Models.SportData;
using System.ComponentModel.DataAnnotations;

namespace Models.SeasonData
{
    public class SeasonDTO
    {
        public int Id { get; set; }
        public bool IsActive { get; set; }
        [Required]
        public string Name { get; set; }
        public int SportId { get; set; }
        public virtual SportDTO Sport { get; set; }
    }
}
