﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class PaginationDTO
    {
        public int Page { get; set; } = 1;
        public int RowsPerPage { get; set; } = 10;
    }
}
