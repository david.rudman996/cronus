﻿using Models.SportData;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Models.PlayerData
{
    public class PlayerDTO
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Surname { get; set; }
        public int SportId { get; set; }
        public virtual SportDTO Sport { get; set; }
    }
}
