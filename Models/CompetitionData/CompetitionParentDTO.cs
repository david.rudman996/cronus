﻿using Models.SeasonData;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.CompetitionData
{
    public class CompetitionParentDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int SeasonId { get; set; }
        public virtual SeasonDTO Season { get; set; }
    }
}
