﻿using Models.TeamData;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.CompetitionData
{
    public class CompetitionTeamDTO
    {
        public int Id { get; set; }
        public int GoalsScored { get; set; }
        public int Draw { get; set; }
        public int GoalsConceded { get; set; }
        public int Played { get; set; }
        public int Won { get; set; }
        public int Lost { get; set; }
        public int Points { get; set; }
        public int Rank { get; set; }
        public bool Disqualified { get; set; }
        public int CompetitionId { get; set; }
        public int TeamId { get; set; }
        public virtual CompetitionDTO Competition { get; set; }
        public virtual TeamDTO Team { get; set; }
    }
}
