﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.CompetitionData
{
    public class CompetitionDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CompetitionParentId { get; set; }
        public virtual CompetitionParentDTO CompetitionParent { get; set; }
    }
}
