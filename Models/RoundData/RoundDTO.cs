﻿using Models.CompetitionData;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.RoundData
{
    public class RoundDTO
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please enter round name")]
        public string Name { get; set; }
        public int CompetitionId { get; set; }
        public virtual CompetitionDTO Competition { get; set; }
    }
}
