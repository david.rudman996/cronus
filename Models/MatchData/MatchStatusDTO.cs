﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.MatchData
{
    public class MatchStatusDTO
    {
        public int Id { get; set; }
        public string Status { get; set; }
    }
}
