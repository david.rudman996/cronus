﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.MatchData
{
    public class MatchCommentaryDTO
    {
        public int Id { get; set; }
        public string Comment { get; set; }
        public int Minute { get; set; }
        public int MatchId { get; set; }
        public virtual MatchCoreDTO Match { get; set; }
    }
}
