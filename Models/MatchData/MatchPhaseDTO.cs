﻿using DataAccess.Data.SportData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.MatchData
{
    public class MatchPhaseDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int SportId { get; set; }
        public virtual Sport Sport { get; set; }
    }
}
