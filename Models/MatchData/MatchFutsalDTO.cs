﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.MatchData
{
    public class MatchFutsalDTO
    {
        public int Id { get; set; }
        public int FoulAway { get; set; }
        public int FoulHome { get; set; }
        public int ResultHalfAway { get; set; }
        public int ResultHalfHome { get; set; }
        public int ResultSecondHalfAway { get; set; }
        public int ResultSecondHalfHome { get; set; }
        public int ResultFullAway { get; set; }
        public int ResultFullHome { get; set; }
        public int ResultPenaltyAway { get; set; }
        public int ResultPenaltyHome { get; set; }
        public int MatchId { get; set; }
        public virtual MatchCoreDTO Match { get; set; }

    }
}
