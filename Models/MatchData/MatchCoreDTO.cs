﻿using Models.CompetitionData;
using Models.PlayerData;
using Models.StadiumData;
using Models.TeamData;
using Models.RoundData;
using System;


namespace Models.MatchData
{
    public class MatchCoreDTO
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public int Minute { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public int TeamAwayScore { get; set; }
        public int TeamHomeScore { get; set; }
        public int StadiumId { get; set; }
        public int CompetitionId { get; set; }
        public int MatchStatusId { get; set; }
        public int TeamAwayId { get; set; }
        public int TeamHomeId { get; set; }
        public int? PlayerOfTheMatchId { get; set; }
        public int RoundId { get; set; }
        public virtual StadiumDTO Stadium { get; set; }

        public virtual CompetitionDTO Competition { get; set; }

        public virtual MatchStatusDTO MatchStatus { get; set; }

        public virtual TeamDTO TeamAway { get; set; }

        public virtual TeamDTO TeamHome { get; set; }
        public virtual PlayerDTO PlayerOfTheMatch { get; set; }
        public virtual RoundDTO Round { get; set; }

    }
}
