﻿using Models.SportData;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Models.TeamData
{
    public class TeamDTO
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string TeamLogoUrl { get; set; }
        public int SportId { get; set; }
        public virtual SportDTO Sport { get; set; }
    }
}
