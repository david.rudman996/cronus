﻿using Models.SeasonData;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Models.TeamData
{
    public class TeamSeasonDTO
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please pick a team")]
        public int TeamId { get; set; }
        public int SeasonId { get; set; }
        public virtual TeamDTO Team { get; set; }
        public virtual SeasonDTO Season { get; set; }
    }
}
