﻿using Models.PlayerData;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.TeamData
{
    public class TeamPlayerDTO
    {
        public int Id { get; set; } 
        public int PlayerId { get; set; }
        public int TeamSeasonId { get; set; }
        public virtual PlayerDTO Player { get; set; }
        public virtual TeamSeasonDTO TeamSeason { get; set; }
    }
}
