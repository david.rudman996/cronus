﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Models.RefereeData
{
    public class RefereeDTO
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please enter referee name")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Please enter referee surname")]
        [RegularExpression(@"^[a-zA-Z''-'\s]{3,40}$", ErrorMessage = "Only letters in length of 3-40 are allowed.")]
        public string Surname { get; set; }
    }
}
