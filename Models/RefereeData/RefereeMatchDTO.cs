﻿using Models.MatchData;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.RefereeData
{
    public class RefereeMatchDTO
    {
        public int Id { get; set; }
        public int MatchCoreId;
        public virtual MatchCoreDTO MatchCore { get; set; }
        public int RefereeId;
        public virtual RefereeDTO Referee { get; set; }
    }
}
