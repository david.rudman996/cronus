#pragma checksum "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Team\TeamSeasonCreateUpdate.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "d84d3b65df23e32f07475a17dac22d5250508366"
// <auto-generated/>
#pragma warning disable 1591
namespace Cronus_Server.Pages.Team
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using Cronus_Server;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using Cronus_Server.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using Cronus_Server.Helper;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using Blazor.ModalDialog;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using Blazored.Modal;

#line default
#line hidden
#nullable disable
#nullable restore
#line 14 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using Blazored.Modal.Services;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using Cronus_Server.Shared.Tabs;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Team\TeamSeasonCreateUpdate.razor"
using Models.TeamData;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Team\TeamSeasonCreateUpdate.razor"
using Models.SeasonData;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Team\TeamSeasonCreateUpdate.razor"
using Business.Repository.IRepository;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
[Authorize(Roles = Business.SD.Role_Admin)]

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/teamSeasons/create")]
    [Microsoft.AspNetCore.Components.RouteAttribute("/teamSeasons/edit/{Id:int}")]
    public partial class TeamSeasonCreateUpdate : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenElement(0, "div");
            __builder.AddAttribute(1, "class", "row mt-2 mb-5");
            __builder.OpenElement(2, "h3");
            __builder.AddAttribute(3, "class", "card-title text-info mb-3 ml-3");
            __builder.AddContent(4, 
#nullable restore
#line 15 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Team\TeamSeasonCreateUpdate.razor"
                                                Title

#line default
#line hidden
#nullable disable
            );
            __builder.AddContent(5, " Team-Season");
            __builder.CloseElement();
            __builder.AddMarkupContent(6, "\r\n    ");
            __builder.OpenElement(7, "div");
            __builder.AddAttribute(8, "class", "col-md-12");
            __builder.OpenElement(9, "div");
            __builder.AddAttribute(10, "class", "card");
            __builder.OpenElement(11, "div");
            __builder.AddAttribute(12, "class", "card-body");
            __builder.OpenComponent<Microsoft.AspNetCore.Components.Forms.EditForm>(13);
            __builder.AddAttribute(14, "Model", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Object>(
#nullable restore
#line 19 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Team\TeamSeasonCreateUpdate.razor"
                                  TeamSeasonModel

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(15, "OnValidSubmit", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<Microsoft.AspNetCore.Components.Forms.EditContext>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Forms.EditContext>(this, 
#nullable restore
#line 19 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Team\TeamSeasonCreateUpdate.razor"
                                                                  HandleCreateUpdate

#line default
#line hidden
#nullable disable
            )));
            __builder.AddAttribute(16, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment<Microsoft.AspNetCore.Components.Forms.EditContext>)((context) => (__builder2) => {
                __builder2.OpenComponent<Microsoft.AspNetCore.Components.Forms.DataAnnotationsValidator>(17);
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(18, "\r\n                    ");
                __builder2.OpenElement(19, "div");
                __builder2.AddAttribute(20, "class", "form-group");
                __builder2.AddMarkupContent(21, "<label>Season</label>\r\n                        ");
                __builder2.OpenComponent<Microsoft.AspNetCore.Components.Forms.InputText>(22);
                __builder2.AddAttribute(23, "class", "form-control");
                __builder2.AddAttribute(24, "disabled", "true");
                __builder2.AddAttribute(25, "Value", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 24 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Team\TeamSeasonCreateUpdate.razor"
                                                currentSeason

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(26, "ValueChanged", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<System.String>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<System.String>(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => currentSeason = __value, currentSeason))));
                __builder2.AddAttribute(27, "ValueExpression", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Linq.Expressions.Expression<System.Func<System.String>>>(() => currentSeason));
                __builder2.CloseComponent();
                __builder2.CloseElement();
                __builder2.AddMarkupContent(28, "\r\n                    ");
                __builder2.OpenElement(29, "div");
                __builder2.AddAttribute(30, "class", "form-group");
                __builder2.AddMarkupContent(31, "<label>Team</label>\r\n                        ");
                __builder2.OpenElement(32, "select");
                __builder2.AddAttribute(33, "class", "form-select");
                __builder2.AddAttribute(34, "value", Microsoft.AspNetCore.Components.BindConverter.FormatValue(
#nullable restore
#line 28 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Team\TeamSeasonCreateUpdate.razor"
                                                           selectedTeamId

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(35, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.CreateBinder(this, __value => selectedTeamId = __value, selectedTeamId));
                __builder2.SetUpdatesAttributeName("value");
                __builder2.OpenElement(36, "option");
                __builder2.AddAttribute(37, "value", "-1");
                __builder2.AddContent(38, "--select--");
                __builder2.CloseElement();
#nullable restore
#line 30 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Team\TeamSeasonCreateUpdate.razor"
                             foreach (var item in Teams)
                            {

#line default
#line hidden
#nullable disable
                __builder2.OpenElement(39, "option");
                __builder2.AddAttribute(40, "class", "p-3");
                __builder2.AddAttribute(41, "value", 
#nullable restore
#line 32 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Team\TeamSeasonCreateUpdate.razor"
                                                            item.Id

#line default
#line hidden
#nullable disable
                );
                __builder2.AddContent(42, 
#nullable restore
#line 32 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Team\TeamSeasonCreateUpdate.razor"
                                                                      item.Name

#line default
#line hidden
#nullable disable
                );
                __builder2.CloseElement();
#nullable restore
#line 33 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Team\TeamSeasonCreateUpdate.razor"
                            }

#line default
#line hidden
#nullable disable
                __builder2.CloseElement();
                __builder2.CloseElement();
                __builder2.AddMarkupContent(43, "\r\n                    ");
                __builder2.OpenElement(44, "div");
                __builder2.AddAttribute(45, "class", "form-group mt-3");
                __builder2.OpenElement(46, "button");
                __builder2.AddAttribute(47, "class", "btn btn-primary");
                __builder2.AddContent(48, 
#nullable restore
#line 37 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Team\TeamSeasonCreateUpdate.razor"
                                                         Title

#line default
#line hidden
#nullable disable
                );
                __builder2.AddContent(49, " Team");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(50, "\r\n                        ");
                __builder2.OpenComponent<Microsoft.AspNetCore.Components.Routing.NavLink>(51);
                __builder2.AddAttribute(52, "href", "teamSeasons");
                __builder2.AddAttribute(53, "class", "btn btn-secondary");
                __builder2.AddAttribute(54, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder3) => {
                    __builder3.AddContent(55, "Back to Index");
                }
                ));
                __builder2.CloseComponent();
                __builder2.CloseElement();
            }
            ));
            __builder.CloseComponent();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
        }
        #pragma warning restore 1998
#nullable restore
#line 46 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Team\TeamSeasonCreateUpdate.razor"
       

    [Parameter]
    public int? Id { get; set; }
    private TeamSeasonDTO TeamSeasonModel { get; set; } = new TeamSeasonDTO();
    private IEnumerable<TeamDTO> Teams { get; set; } = new List<TeamDTO>();
    private int selectedTeamId { get; set; }
    private string Title { get; set; } = "Register";
    private string currentSport { get; set; }
    private string currentSeason { get; set; }
    private int currentSeasonId { get; set; }
    protected override async Task OnInitializedAsync()
    {
        //currentSport = await localStorage.GetItemAsync<string>("sportName");
        currentSeason = await localStorage.GetItemAsync<string>("seasonName");
        currentSeasonId = await localStorage.GetItemAsync<int>("seasonId");
        if (Id != null)
        {
            //update
            Title = "Update";
            TeamSeasonModel = await TeamSeasonRepository.Get(Id.Value);

        }
        else
        {
            //create
            TeamSeasonModel = new TeamSeasonDTO();
            //Teams = await TeamRepository.GetAllFromSport(await localStorage.GetItemAsync<int>("sportId"));
            Teams = await TeamRepository.GetAllAvailableTeamsForTeamSeason
                (currentSeasonId, await localStorage.GetItemAsync<int>("sportId"));
        }
    }
    private async Task HandleCreateUpdate()
    {
        try
        {
            // add check if teamSeason is unique
            // --
            // --
            if (TeamSeasonModel.Id != 0 && Title == "Update")
            {
                //update
                var updateTeamSeason = await TeamSeasonRepository.Update(TeamSeasonModel, TeamSeasonModel.Id);
                await JsRuntime.ToastrSuccess("TeamSeason updated successfully");
                await Task.Delay(300);
                NavigationManager.NavigateTo("/teamSeasons", true);

            }
            else
            {
                //create

                TeamSeasonModel.SeasonId = currentSeasonId;
                TeamSeasonModel.TeamId = selectedTeamId;
                var createdResult = await TeamSeasonRepository.Create(TeamSeasonModel);

                await JsRuntime.ToastrSuccess("TeamSeason created successfully");
                await Task.Delay(300);
                NavigationManager.NavigateTo("/teamSeasons", true);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private Blazored.LocalStorage.ILocalStorageService localStorage { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private ITeamRepository TeamRepository { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private ISeasonRepository SeasonRepository { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private ITeamSeasonRepository TeamSeasonRepository { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IJSRuntime JsRuntime { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private NavigationManager NavigationManager { get; set; }
    }
}
#pragma warning restore 1591
