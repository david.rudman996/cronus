#pragma checksum "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Match\MatchList.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "9315b017c2aab4ed689c633a14acb82eb97e0360"
// <auto-generated/>
#pragma warning disable 1591
namespace Cronus_Server.Pages.Match
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using Cronus_Server;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using Cronus_Server.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using Cronus_Server.Helper;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using Blazor.ModalDialog;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using Blazored.Modal;

#line default
#line hidden
#nullable disable
#nullable restore
#line 14 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using Blazored.Modal.Services;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using Cronus_Server.Shared.Tabs;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Match\MatchList.razor"
using Models.MatchData;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Match\MatchList.razor"
using Business.Repository.IRepository;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
[Authorize(Roles = Business.SD.Role_Admin)]

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/matches")]
    public partial class MatchList : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenElement(0, "div");
            __builder.AddAttribute(1, "class", "row mt-4");
            __builder.AddMarkupContent(2, "<div class=\"col-8\"><h4 class=\"card-title text-info\">Matches</h4></div>\r\n    ");
            __builder.OpenElement(3, "div");
            __builder.AddAttribute(4, "class", "col-3 offset-1");
            __builder.OpenComponent<Microsoft.AspNetCore.Components.Routing.NavLink>(5);
            __builder.AddAttribute(6, "href", 
#nullable restore
#line 16 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Match\MatchList.razor"
                         $"/matches/create"

#line default
#line hidden
#nullable disable
            );
            __builder.AddAttribute(7, "class", "btn btn-primary form-control");
            __builder.AddAttribute(8, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder2) => {
                __builder2.AddContent(9, "Add new match");
            }
            ));
            __builder.CloseComponent();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.AddMarkupContent(10, "\r\n\r\n");
            __builder.OpenElement(11, "div");
            __builder.AddAttribute(12, "class", "row mt-2 mb-5");
            __builder.OpenElement(13, "div");
            __builder.AddAttribute(14, "class", "col-md-12");
            __builder.OpenElement(15, "div");
            __builder.AddAttribute(16, "class", "card");
            __builder.OpenElement(17, "div");
            __builder.AddAttribute(18, "class", "card-body");
#nullable restore
#line 25 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Match\MatchList.razor"
                 if (Matches.Any())
                {
                    

#line default
#line hidden
#nullable disable
#nullable restore
#line 27 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Match\MatchList.razor"
                     foreach (var match in Matches)
                    {
                        

#line default
#line hidden
#nullable disable
            __builder.OpenElement(19, "div");
            __builder.AddAttribute(20, "class", "row align-items-center text-center");
            __builder.AddAttribute(21, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 30 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Match\MatchList.razor"
                                                                                   () => NavigateToDetails(match.Id)

#line default
#line hidden
#nullable disable
            ));
            __builder.OpenElement(22, "div");
            __builder.AddAttribute(23, "class", "col-5");
            __builder.OpenElement(24, "p");
            __builder.AddAttribute(25, "class", "h5 text-secondary text-center");
            __builder.OpenElement(26, "strong");
            __builder.AddContent(27, 
#nullable restore
#line 32 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Match\MatchList.razor"
                                                                                  match.TeamHome.Name

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.AddMarkupContent(28, "\r\n                            ");
            __builder.OpenElement(29, "div");
            __builder.AddAttribute(30, "class", "col-2");
#nullable restore
#line 35 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Match\MatchList.razor"
                                 if (match.MatchStatus.Status.Equals("Scheduled"))
                                {

#line default
#line hidden
#nullable disable
            __builder.OpenElement(31, "div");
            __builder.AddAttribute(32, "class", "row align-items-center");
            __builder.OpenElement(33, "div");
            __builder.AddAttribute(34, "class", "col-12");
            __builder.AddContent(35, 
#nullable restore
#line 39 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Match\MatchList.razor"
                                             match.Competition.Name

#line default
#line hidden
#nullable disable
            );
            __builder.AddContent(36, " - ");
            __builder.AddContent(37, 
#nullable restore
#line 39 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Match\MatchList.razor"
                                                                       match.Round.Name

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(38, "\r\n                                        ");
            __builder.OpenElement(39, "div");
            __builder.AddAttribute(40, "class", "col-12");
            __builder.AddContent(41, 
#nullable restore
#line 42 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Match\MatchList.razor"
                                             match.Date.ToString("dd.MM.yyyy")

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(42, "\r\n                                        ");
            __builder.OpenElement(43, "div");
            __builder.AddAttribute(44, "class", "col-12");
            __builder.AddContent(45, 
#nullable restore
#line 45 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Match\MatchList.razor"
                                             match.Date.ToString("HH:mm")

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.CloseElement();
#nullable restore
#line 48 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Match\MatchList.razor"
                                }
                                else if (match.MatchStatus.Status.Equals("Finished"))
                                {

#line default
#line hidden
#nullable disable
            __builder.OpenElement(46, "div");
            __builder.AddAttribute(47, "class", "row align-items-center");
            __builder.OpenElement(48, "div");
            __builder.AddAttribute(49, "class", "col-12");
            __builder.AddContent(50, 
#nullable restore
#line 53 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Match\MatchList.razor"
                                             match.Competition.Name

#line default
#line hidden
#nullable disable
            );
            __builder.AddContent(51, " - ");
            __builder.AddContent(52, 
#nullable restore
#line 53 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Match\MatchList.razor"
                                                                       match.Round.Name

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(53, "\r\n                                        ");
            __builder.OpenElement(54, "div");
            __builder.AddAttribute(55, "class", "col-12");
            __builder.OpenElement(56, "p");
            __builder.AddAttribute(57, "class", "text-warning h5");
            __builder.OpenElement(58, "strong");
            __builder.AddContent(59, 
#nullable restore
#line 56 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Match\MatchList.razor"
                                                                                match.TeamHomeScore

#line default
#line hidden
#nullable disable
            );
            __builder.AddContent(60, " : ");
            __builder.AddContent(61, 
#nullable restore
#line 56 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Match\MatchList.razor"
                                                                                                       match.TeamAwayScore

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.AddMarkupContent(62, "\r\n                                        ");
            __builder.OpenElement(63, "div");
            __builder.AddAttribute(64, "class", "col-12");
            __builder.AddContent(65, 
#nullable restore
#line 59 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Match\MatchList.razor"
                                             match.Date.ToString("dd.MM.yyyy")

#line default
#line hidden
#nullable disable
            );
            __builder.AddContent(66, " - ");
            __builder.AddContent(67, 
#nullable restore
#line 59 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Match\MatchList.razor"
                                                                                  match.Date.ToString("HH:mm")

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.CloseElement();
#nullable restore
#line 62 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Match\MatchList.razor"
                                }
                                else if (match.MatchStatus.Status.Equals("Live"))
                                {

#line default
#line hidden
#nullable disable
            __builder.OpenElement(68, "div");
            __builder.AddAttribute(69, "class", "row align-items-center");
            __builder.OpenElement(70, "div");
            __builder.AddAttribute(71, "class", "col-12 ");
            __builder.OpenElement(72, "span");
            __builder.AddAttribute(73, "style", "color: #3d9c27");
            __builder.AddContent(74, "Live: ");
            __builder.AddContent(75, 
#nullable restore
#line 68 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Match\MatchList.razor"
                                                                                match.Minute

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(76, "\'\r\n                                        ");
            __builder.CloseElement();
            __builder.AddMarkupContent(77, "\r\n                                        ");
            __builder.OpenElement(78, "div");
            __builder.AddAttribute(79, "class", "col-12");
            __builder.AddContent(80, 
#nullable restore
#line 71 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Match\MatchList.razor"
                                             match.Competition.Name

#line default
#line hidden
#nullable disable
            );
            __builder.AddContent(81, " - ");
            __builder.AddContent(82, 
#nullable restore
#line 71 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Match\MatchList.razor"
                                                                       match.Round.Name

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(83, "\r\n                                        ");
            __builder.OpenElement(84, "div");
            __builder.AddAttribute(85, "class", "col-12");
            __builder.OpenElement(86, "p");
            __builder.AddAttribute(87, "class", "text-warning h5");
            __builder.OpenElement(88, "strong");
            __builder.AddContent(89, 
#nullable restore
#line 74 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Match\MatchList.razor"
                                                                                match.TeamHomeScore

#line default
#line hidden
#nullable disable
            );
            __builder.AddContent(90, " : ");
            __builder.AddContent(91, 
#nullable restore
#line 74 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Match\MatchList.razor"
                                                                                                       match.TeamAwayScore

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.AddMarkupContent(92, "\r\n                                        ");
            __builder.OpenElement(93, "div");
            __builder.AddAttribute(94, "class", "col-12");
            __builder.AddContent(95, 
#nullable restore
#line 77 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Match\MatchList.razor"
                                             match.Date.ToString("dd.MM.yyyy")

#line default
#line hidden
#nullable disable
            );
            __builder.AddContent(96, " - ");
            __builder.AddContent(97, 
#nullable restore
#line 77 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Match\MatchList.razor"
                                                                                  match.Date.ToString("HH:mm")

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.CloseElement();
#nullable restore
#line 80 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Match\MatchList.razor"
                                }

#line default
#line hidden
#nullable disable
            __builder.CloseElement();
            __builder.AddMarkupContent(98, "\r\n                            ");
            __builder.OpenElement(99, "div");
            __builder.AddAttribute(100, "class", "col-5");
            __builder.OpenElement(101, "p");
            __builder.AddAttribute(102, "class", "h5 text-secondary text-center");
            __builder.OpenElement(103, "strong");
            __builder.AddContent(104, 
#nullable restore
#line 83 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Match\MatchList.razor"
                                                                                  match.TeamAway.Name

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
#nullable restore
#line 86 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Match\MatchList.razor"
                         if (Matches.Count() > index)
                        {
                            index++;

#line default
#line hidden
#nullable disable
            __builder.AddMarkupContent(105, "<hr>");
#nullable restore
#line 90 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Match\MatchList.razor"
                        }

#line default
#line hidden
#nullable disable
#nullable restore
#line 90 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Match\MatchList.razor"
                         
                    }

#line default
#line hidden
#nullable disable
#nullable restore
#line 91 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Match\MatchList.razor"
                     
                }
                else
                {

#line default
#line hidden
#nullable disable
            __builder.AddMarkupContent(106, "<div class=\"row\"><div class=\"col-5\">No records found</div></div>");
#nullable restore
#line 98 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Match\MatchList.razor"
                }

#line default
#line hidden
#nullable disable
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.AddMarkupContent(107, "\r\n\r\n\r\n");
            __builder.OpenComponent<Cronus_Server.Shared.DeleteConfirmation>(108);
            __builder.AddAttribute(109, "IsParentComponentProcessing", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 106 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Match\MatchList.razor"
                                                 IsProcessing

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(110, "ConfirmationChanged", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<System.Boolean>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<System.Boolean>(this, 
#nullable restore
#line 106 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Match\MatchList.razor"
                                                                                    ConfirmDelete_Click

#line default
#line hidden
#nullable disable
            )));
            __builder.CloseComponent();
        }
        #pragma warning restore 1998
#nullable restore
#line 108 "C:\Users\David\source\repos\Cronus\Cronus_Server\Pages\Match\MatchList.razor"
       
    private IEnumerable<MatchCoreDTO> Matches { get; set; } = new List<MatchCoreDTO>();
    private int? DeleteMatchId { get; set; } = null;
    private bool IsProcessing { get; set; } = false;
    private string CurrentSport { get; set; }
    private int CurrentSportId { get; set; }
    private int CurrentSeasonId { get; set; }
    private int index = 1;

    protected async override Task OnInitializedAsync()
    {
        CurrentSport = await localStorage.GetItemAsync<string>("sportName");
        CurrentSportId = await localStorage.GetItemAsync<int>("sportId");
        CurrentSeasonId = await localStorage.GetItemAsync<int>("seasonId");
        Matches = await MatchCoreRepository.GetAllMatchesFromSport(CurrentSportId, CurrentSeasonId);
    }

    private async Task HandleDelete(int matchId)
    {
        DeleteMatchId = matchId;
        await JsRuntime.InvokeVoidAsync("ShowDeleteConfirmationModal");
    }
    public async Task ConfirmDelete_Click(bool isConfirmed)
    {
        IsProcessing = true;
        if (isConfirmed && DeleteMatchId != null)
        {
            await MatchFutsalRepository.Delete(DeleteMatchId.Value);
            await JsRuntime.ToastrSuccess("Match deleted successfully!");
            Matches = await MatchCoreRepository.GetAllMatchesFromSport(CurrentSportId, CurrentSeasonId);
        }
        await JsRuntime.InvokeVoidAsync("HideDeleteConfirmationModal");
        IsProcessing = false;
    }
    public void NavigateToDetails(int matchCoreId)
    {
        NavigationManager.NavigateTo("/matches/"+ CurrentSport + "/" + matchCoreId, true);
    }

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private NavigationManager NavigationManager { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private Blazored.LocalStorage.ILocalStorageService localStorage { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IJSRuntime JsRuntime { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IMatchCoreRepository MatchCoreRepository { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IMatchFutsalRepository MatchFutsalRepository { get; set; }
    }
}
#pragma warning restore 1591
