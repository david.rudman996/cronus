// <auto-generated/>
#pragma warning disable 1591
#pragma warning disable 0414
#pragma warning disable 0649
#pragma warning disable 0169

namespace Cronus_Server.Shared
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using Cronus_Server;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using Cronus_Server.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using Cronus_Server.Helper;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using Blazor.ModalDialog;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using Blazored.Modal;

#line default
#line hidden
#nullable disable
#nullable restore
#line 14 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using Blazored.Modal.Services;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
using Cronus_Server.Shared.Tabs;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\David\source\repos\Cronus\Cronus_Server\Shared\MainLayout.razor"
using Models.SeasonData;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\David\source\repos\Cronus\Cronus_Server\Shared\MainLayout.razor"
using Business.Repository.IRepository;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "C:\Users\David\source\repos\Cronus\Cronus_Server\_Imports.razor"
[Authorize(Roles = Business.SD.Role_Admin)]

#line default
#line hidden
#nullable disable
    public partial class MainLayout : LayoutComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
        }
        #pragma warning restore 1998
#nullable restore
#line 36 "C:\Users\David\source\repos\Cronus\Cronus_Server\Shared\MainLayout.razor"
      
    private IEnumerable<SeasonDTO> Seasons { get; set; } = new List<SeasonDTO>();
    private SeasonDTO ActiveSeason { get; set; } = new SeasonDTO();
    private string btn1 { get; set; } = "btn";
    private string btn2 { get; set; } = "btn";
    private string sport = "";
    private int? sportId;

    protected override async Task OnInitializedAsync()
    {
        sport = await localStorage.GetItemAsync<string>("sportName");
        sportId = await localStorage.GetItemAsync<int>("sportId");

        if (sportId != 0)
        {
            Seasons = await SeasonRepository.GetAllFromSport(sportId.Value);
            if (Seasons != null)
            {
                ActiveSeason = Seasons.Where(x => x.IsActive == true).SingleOrDefault();
            }
            if (ActiveSeason != null)
            {
                await localStorage.SetItemAsync("seasonName", ActiveSeason.Name);
                await localStorage.SetItemAsync("seasonId", ActiveSeason.Id);
            }
            else
            {
                await localStorage.RemoveItemAsync("seasonName");
                await localStorage.RemoveItemAsync("seasonId");
            }
        }

        if (sport != null && sport.Equals("Futsal"))
        {
            btn1 = btn1 + " active";
        }
        else if (sport != null && sport.Equals("Basketball"))
        {
            btn2 = btn2 + " active";
        }
    }

    private async void Click1()
    {
        await localStorage.RemoveItemAsync("sportName");
        await localStorage.SetItemAsync("sportName", "Futsal");
        await localStorage.RemoveItemAsync("sportId");
        await localStorage.SetItemAsync("sportId", 1);
        btn1 = btn1 + " active";
        btn2 = "btn";
        NavigationManager.NavigateTo("/", true);

    }
    private async void Click2()
    {
        await localStorage.RemoveItemAsync("sportName");
        await localStorage.SetItemAsync("sportName", "Basketball");
        await localStorage.RemoveItemAsync("sportId");
        await localStorage.SetItemAsync("sportId", 2);
        btn2 = btn2 + " active";
        btn1 = "btn";
        NavigationManager.NavigateTo("/", true);
    }

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private ISeasonRepository SeasonRepository { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private NavigationManager NavigationManager { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private Blazored.LocalStorage.ILocalStorageService localStorage { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IJSRuntime JsRuntime { get; set; }
    }
}
#pragma warning restore 1591
