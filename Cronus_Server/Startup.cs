using Blazor.ModalDialog;
using Blazored.LocalStorage;
using Blazored.Modal;
using Business.Repository;
using Business.Repository.IRepository;
using Cronus_Server.Data;
using Cronus_Server.Service.IService;
using Cronus_Server.Service;
using DataAccess.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cronus_Server
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>().AddDefaultTokenProviders()
                .AddDefaultUI();

            services.AddBlazoredLocalStorage();
            services.AddBlazoredLocalStorage(config =>
                config.JsonSerializerOptions.WriteIndented = true);
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            services.AddModalDialog();
            services.AddBlazoredModal();
            services.AddScoped<ICompetitionParentRepository, CompetitionParentRepository>();
            services.AddScoped<ICompetitionRepository, CompetitionRepository>();
            services.AddScoped<ICompetitionTeamRepository, CompetitionTeamRepository>();
            services.AddScoped<IMatchBasketballRepository, MatchBasketballRepository>();
            services.AddScoped<IMatchCommentaryRepository, MatchCommentaryRepository>();
            services.AddScoped<IMatchCoreRepository, MatchCoreRepository>();
            services.AddScoped<IMatchFutsalRepository, MatchFutsalRepository>();
            services.AddScoped<IMatchStatusRepository, MatchStatusRepository>();
            services.AddScoped<IPlayerRepository, PlayerRepository>();
            services.AddScoped<IRoundRepository, RoundRepository>();
            services.AddScoped<IRefereeMatchRepository, RefereeMatchRepository>();
            services.AddScoped<IRefereeRepository, RefereeRepository>();
            services.AddScoped<IStadiumRepository, StadiumRepository>();
            services.AddScoped<ISportRepository, SportRepository>();
            services.AddScoped<ISeasonRepository, SeasonRepository>();
            services.AddScoped<ISportRepository, SportRepository>();
            services.AddScoped<ITeamPlayerRepository, TeamPlayerRepository>();
            services.AddScoped<ITeamRepository, TeamRepository>();
            services.AddScoped<ITeamSeasonRepository, TeamSeasonRepository>();

            services.AddScoped<IDbInitializer, DbInitializer>();
           
            services.AddRazorPages();
            services.AddServerSideBlazor();
            services.AddSingleton<WeatherForecastService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IDbInitializer dbInitializer)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting(); // routing must happen before authentication and authorization

            app.UseAuthentication();
            app.UseAuthorization();
            dbInitializer.Initialize();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }
}
