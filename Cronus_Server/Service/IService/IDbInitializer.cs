﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cronus_Server.Service.IService
{
    public interface IDbInitializer
    {
        void Initialize();
    }
}
