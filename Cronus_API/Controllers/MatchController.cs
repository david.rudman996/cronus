﻿using Microsoft.AspNetCore.Mvc;
using Business.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Models;
using Microsoft.AspNetCore.Http;

namespace Cronus_API.Controllers
{
    [Route("matches")]
    public class MatchController : Controller
    {
        private readonly IMatchCoreRepository _matchCoreRepository;

        public MatchController(IMatchCoreRepository matchCoreRepository)
        {
            _matchCoreRepository = matchCoreRepository;
        }

        [HttpGet("{sportId}/{seasonId}")]
        public async Task<IActionResult> GetMatches(int? sportId, int? seasonId)
        {
            if (sportId == null || seasonId == null)
            {
                return BadRequest(new ErrorModel()
                {
                    Title = "",
                    ErrorMessage = "Invalid Ids",
                    StatusCode = StatusCodes.Status400BadRequest
                });
            }
            var allMatches = await _matchCoreRepository.GetAllMatchesFromSport(sportId.Value, seasonId.Value);
            return Ok(allMatches);
        }
    }
}
