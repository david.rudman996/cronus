﻿using DataAccess.Data.TeamData;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccess.Data.CompetitionData
{
    public class CompetitionTeam
    {
        [Key]
        public int Id { get; set; }
        public int GoalsScored { get; set; }
        public int Draw { get; set; }
        public int GoalsConceded { get; set; }
        public int Played { get; set; }
        public int Won { get; set; }
        public int Lost { get; set; }
        public int Points { get; set; }
        public int Rank { get; set; }
        public bool Disqualified { get; set; }
        public int? CompetitionId { get; set; }
        public int? TeamId { get; set; }
        [ForeignKey("CompetitionId")]
        public virtual Competition Competition { get; set; }
        [ForeignKey("TeamId")]
        public virtual Team Team { get; set; }


    }
}
