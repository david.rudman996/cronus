﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using DataAccess.Data.SeasonData;

namespace DataAccess.Data.CompetitionData
{
    public class CompetitionParent
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public int? SeasonId { get; set; }

        [ForeignKey("SeasonId")]
        public virtual Season Season { get; set; }
    }
}
