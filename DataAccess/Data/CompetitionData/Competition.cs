﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccess.Data.CompetitionData
{
    public class Competition
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public int? CompetitionParentId { get; set; }
        public virtual CompetitionParent CompetitionParent {get; set;}

    }
}
