﻿using DataAccess.Data.CompetitionData;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Data.RoundData
{
    public class Round
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public int? CompetitionId { get; set; }
        [ForeignKey("CompetitionId")]
        public virtual Competition Competition { get; set; }
    }
}
