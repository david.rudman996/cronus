﻿using DataAccess.Data.SeasonData;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccess.Data.TeamData
{
    public class TeamSeason
    {
        [Key]
        public int Id { get; set; }
        public int? SeasonId { get; set; }
        public int? TeamId { get; set; }

        [ForeignKey("TeamId")]
        public virtual Team Team { get; set; }
        
        [ForeignKey("SeasonId")]
        public virtual Season Season { get; set; }
    }
}
