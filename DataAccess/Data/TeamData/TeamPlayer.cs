﻿using DataAccess.Data.PlayerData;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccess.Data.TeamData
{
    public class TeamPlayer
    {
        [Key]
        public int Id { get; set; }
        public int PlayerId { get; set; }

        public int TeamSeasonId { get; set; }
        [ForeignKey("PlayerId")]
        public virtual Player Player { get; set; }
        [ForeignKey("TeamSeasonId")]
        public virtual TeamSeason TeamSeason { get; set; }
    }
}
