﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccess.Data.MatchData
{
    public class MatchFutsal
    {
        [Key]
        public int Id { get; set; }
        public int FoulAway { get; set; }
        public int FoulHome { get; set; }
        public int ResultHalfAway { get; set; }
        public int ResultHalfHome { get; set; }
        public int ResultSecondHalfAway { get; set; }
        public int ResultSecondHalfHome { get; set; }
        public int ResultFullAway { get; set; }
        public int ResultFullHome { get; set; }
        public int ResultPenaltyAway { get; set; }
        public int ResultPenaltyHome { get; set; }
        public int? MatchId { get; set; }
        [ForeignKey("MatchId")]
        public virtual MatchCore Match { get; set; }

    }
}
