﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Data.MatchData
{
    public class MatchLineup
    {
        [Key]
        public int Id { get; set; }
        public int MatchId { get; set; }
        public int PlayerId { get; set; }
        [ForeignKey("MatchId")]
        public virtual MatchCore Match { get; set; }
    }
}
