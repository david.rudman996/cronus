﻿using DataAccess.Data.CompetitionData;
using DataAccess.Data.PlayerData;
using DataAccess.Data.RoundData;
using DataAccess.Data.StadiumData;
using DataAccess.Data.TeamData;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccess.Data.MatchData
{
    public class MatchCore
    {
        [Key]
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public int Minute { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public int TeamAwayScore { get; set; }
        public int TeamHomeScore { get; set; }
        public int? StadiumId { get; set; }
        public int? CompetitionId { get; set; }
        public int? MatchStatusId { get; set; }
        public int? TeamAwayId { get; set; }
        public int? TeamHomeId { get; set; }
        public int? RoundId { get; set; }
        public int? PlayerOfTheMatchId { get; set; }

        [ForeignKey("StadiumId")]
        public virtual Stadium Stadium { get; set; }

        [ForeignKey("CompetitionId")]
        public virtual Competition Competition { get; set; }

        [ForeignKey("MatchStatusId")]
        public virtual MatchStatus MatchStatus{ get; set; }

        [ForeignKey("TeamAwayId")]
        public virtual Team TeamAway { get; set; }

        [ForeignKey("TeamHomeId")]
        public virtual Team TeamHome { get; set; }

        [ForeignKey("PlayerOfTheMatchId")]
        public virtual Player PlayerOfTheMatch { get; set; }

        [ForeignKey("RoundId")]
        public virtual Round Round { get; set; }
    }
}
