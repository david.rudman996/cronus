﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccess.Data.MatchData
{
    public class MatchCommentary
    {
        [Key]
        public int Id { get; set; }
        public string  Comment { get; set; }
        public int Minute { get; set; }
        public int? MatchId { get; set; }
        [ForeignKey("MatchId")]
        public virtual MatchCore Match { get; set; }
    }
}
