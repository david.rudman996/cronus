﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccess.Data.MatchData
{
    public class MatchStatus
    {
        [Key]
        public int Id { get; set; }
        public string Status { get; set; }
    }
}
