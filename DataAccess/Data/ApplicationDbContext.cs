﻿using DataAccess.Data.CompetitionData;
using DataAccess.Data.MatchData;
using DataAccess.Data.PlayerData;
using DataAccess.Data.RefereeData;
using DataAccess.Data.RoundData;
using DataAccess.Data.SeasonData;
using DataAccess.Data.SportData;
using DataAccess.Data.StadiumData;
using DataAccess.Data.TeamData;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }

        public DbSet<CompetitionParent> CompetitionParents { get; set; }
        public DbSet<Competition> Competitions { get; set; }
        public DbSet<CompetitionTeam> CompetitionTeams { get; set; }
        public DbSet<Season> Seasons { get; set; }
        public DbSet<Sport> Sports { get; set; }
        public DbSet<Player> Players { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<TeamPlayer> TeamPlayers { get; set; }
        public DbSet<TeamSeason> TeamSeasons { get; set; }
        public DbSet<Stadium> Stadiums { get; set; }
        public DbSet<Referee> Referees { get; set; }
        public DbSet<RefereeMatch> RefereeMatches { get; set; }
        public DbSet<MatchCore> Matches { get; set; }
        public DbSet<MatchFutsal> MatchFutsals { get; set; }
        public DbSet<MatchBasketball> MatchBasketballs { get; set; }
        public DbSet<MatchStatus> MatchStatuses { get; set; }
        public DbSet<MatchCommentary> MatchCommentaries { get; set; }
        public DbSet<Round> Rounds { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            #region PostSeed
            modelBuilder.Entity<Sport>().HasData(
                new Sport { Id = 1, Name ="Futsal"},
                new Sport { Id = 2, Name = "Basketball" });

            modelBuilder.Entity<MatchStatus>().HasData(
               new MatchStatus { Id = 1, Status = "Live" },
               new MatchStatus { Id = 2, Status = "Scheduled" },
                new MatchStatus { Id = 3, Status = "Finished" });

            //next is test data that needs to be deleted before production
            modelBuilder.Entity<Season>().HasData(
               new Season { Id = 1, Name = "2020/2021", IsActive = true, SportId = 1});

            modelBuilder.Entity<CompetitionParent>().HasData(
               new CompetitionParent { Id = 1, Name = "Zg futsal liga", SeasonId = 1 });

            modelBuilder.Entity<Competition>().HasData(
               new Competition { Id = 1, Name = "Skupina A", CompetitionParentId = 1 });

            modelBuilder.Entity<Team>().HasData(
               new Team { Id = 1, Name = "Albatrosi", SportId = 1 },
               new Team { Id = 10, Name = "NotAlbatrosi", SportId = 1 });

            #endregion

        }
    }
}
