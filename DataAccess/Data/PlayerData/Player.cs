﻿using DataAccess.Data.SportData;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccess.Data.PlayerData
{
    public class Player
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public int? SportId { get; set; }
        [ForeignKey("SportId")]
        public virtual Sport Sport{ get; set; }
    }
}
