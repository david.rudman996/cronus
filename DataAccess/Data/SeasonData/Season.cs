﻿using DataAccess.Data.SportData;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccess.Data.SeasonData
{
    public class Season
    {
        [Key]
        public int Id { get; set; }
        public bool IsActive { get; set; }
        public string Name { get; set; }
        public int? SportId { get; set; }
        public virtual Sport Sport { get; set; }
    }
}
