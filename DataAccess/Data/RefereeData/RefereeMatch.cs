﻿using DataAccess.Data.MatchData;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccess.Data.RefereeData
{
    public class RefereeMatch
    {
        [Key]
        public int Id { get; set; }
        public int? MatchCoreId { get; set; }
        public int? RefereeId { get; set; }
        [ForeignKey("MatchCoreId")]
        public virtual MatchCore MatchCore { get; set; }
        [ForeignKey("RefereeId")]
        public virtual Referee Referee { get; set; }

    }
}
