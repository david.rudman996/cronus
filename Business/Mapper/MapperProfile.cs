﻿using AutoMapper;
using DataAccess.Data.CompetitionData;
using DataAccess.Data.MatchData;
using DataAccess.Data.PlayerData;
using DataAccess.Data.RefereeData;
using DataAccess.Data.RoundData;
using DataAccess.Data.SeasonData;
using DataAccess.Data.SportData;
using DataAccess.Data.StadiumData;
using DataAccess.Data.TeamData;
using Models.CompetitionData;
using Models.MatchData;
using Models.PlayerData;
using Models.RefereeData;
using Models.RoundData;
using Models.SeasonData;
using Models.SportData;
using Models.StadiumData;
using Models.TeamData;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Mapper
{
    class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<Competition, CompetitionDTO>().ReverseMap();
            CreateMap<CompetitionParent, CompetitionParentDTO>().ReverseMap();
            CreateMap<CompetitionTeam, CompetitionTeamDTO>().ReverseMap();

            CreateMap<MatchBasketball, MatchBasketballDTO>().ReverseMap();
            CreateMap<MatchCommentary, MatchCommentaryDTO>().ReverseMap();
            CreateMap<MatchCore, MatchCoreDTO>();
            CreateMap<MatchCoreDTO, MatchCore>();
            CreateMap<MatchFutsal, MatchFutsalDTO>().ReverseMap();
            CreateMap<MatchStatus, MatchStatusDTO>().ReverseMap();

            CreateMap<Player, PlayerDTO>().ReverseMap();

            CreateMap<Referee, RefereeDTO>().ReverseMap();
            CreateMap<RefereeMatch, RefereeMatchDTO>().ReverseMap();

            CreateMap<Season, SeasonDTO>().ReverseMap();

            CreateMap<Sport, SportDTO>().ReverseMap();

            CreateMap<Round, RoundDTO>().ReverseMap();

            CreateMap<Stadium, StadiumDTO>().ReverseMap();

            CreateMap<Team, TeamDTO>().ReverseMap();
            CreateMap<TeamPlayer, TeamPlayerDTO>().ReverseMap();
            CreateMap<TeamSeason, TeamSeasonDTO>().ReverseMap();
        }
    }
}
