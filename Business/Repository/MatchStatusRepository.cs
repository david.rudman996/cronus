﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Business.Repository.IRepository;
using DataAccess.Data;
using DataAccess.Data.MatchData;
using Microsoft.EntityFrameworkCore;
using Models.MatchData;

namespace Business.Repository
{
    public class MatchStatusRepository : IMatchStatusRepository
    {
        private readonly ApplicationDbContext _db;

        private readonly IMapper _mapper;

        public MatchStatusRepository(ApplicationDbContext db, IMapper mapper)
        {
            _mapper = mapper;
            _db = db;
        }

        public async Task<MatchStatusDTO> Get(int matchStatusId)
        {
            try
            {
                MatchStatusDTO matchStatus = _mapper.Map<MatchStatus, MatchStatusDTO>(
                await _db.MatchStatuses.SingleOrDefaultAsync(x => x.Id == matchStatusId));
                if (matchStatus != null)
                {
                    return matchStatus;
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
