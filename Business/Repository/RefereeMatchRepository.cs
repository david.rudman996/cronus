﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Business.Repository.IRepository;
using DataAccess.Data;
using DataAccess.Data.RefereeData;
using Microsoft.EntityFrameworkCore;
using Models.RefereeData;

namespace Business.Repository
{
    public class RefereeMatchRepository : IRefereeMatchRepository
    {
        private readonly ApplicationDbContext _db;

        private readonly IMapper _mapper;

        public RefereeMatchRepository(ApplicationDbContext db, IMapper mapper)
        {
            _mapper = mapper;
            _db = db;
        }

        public async Task<RefereeMatchDTO> Get(int refereeMatchId)
        {
            try
            {
                RefereeMatchDTO refereeMatch = _mapper.Map<RefereeMatch, RefereeMatchDTO>(
                await _db.RefereeMatches
                .Include(x => x.Referee)
                .Include(x => x.MatchCore)
                .SingleOrDefaultAsync(x => x.Id == refereeMatchId));

                if (refereeMatch != null)
                {
                    return refereeMatch;
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public async Task<RefereeMatchDTO> GetFromMatch(int matchCoreId)
        {
            try
            {
                RefereeMatchDTO refereeMatch = _mapper.Map<RefereeMatch, RefereeMatchDTO>(
                await _db.RefereeMatches
                .Include(x => x.Referee)
                .Include(x => x.MatchCore)
                .SingleOrDefaultAsync(x => x.MatchCore.Id == matchCoreId));

                if (refereeMatch != null)
                {
                    return refereeMatch;
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public async Task<RefereeMatchDTO> Create(RefereeMatchDTO refereeMatchDTO)
        {
            RefereeMatch refereeMatch = _mapper.Map<RefereeMatchDTO, RefereeMatch>(refereeMatchDTO);
            var addedRefereeMatches = await _db.RefereeMatches.AddAsync(refereeMatch);
            await _db.SaveChangesAsync();
            return _mapper.Map<RefereeMatch, RefereeMatchDTO>(addedRefereeMatches.Entity);
        }

        public async Task<RefereeMatchDTO> Update(RefereeMatchDTO refereeMatchDTO, int refereeMatchId)
        {
            try
            {
                if (refereeMatchId == refereeMatchDTO.Id)
                {
                    RefereeMatch refereeMatchDetails = await _db.RefereeMatches.FindAsync(refereeMatchId);
                    RefereeMatch refereeMatch = _mapper.Map<RefereeMatchDTO, RefereeMatch>(refereeMatchDTO, refereeMatchDetails);
                    var updatedRefereeMatch = _db.RefereeMatches.Update(refereeMatch);
                    await _db.SaveChangesAsync();
                    return _mapper.Map<RefereeMatch, RefereeMatchDTO>(updatedRefereeMatch.Entity);
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                return null;
            }
        }
        public async Task<int> Delete(int refereeMatchId)
        {
            var refereeDetails = await _db.RefereeMatches.FindAsync(refereeMatchId);
            if (refereeDetails != null)
            {
                _db.RefereeMatches.Remove(refereeDetails);
                return await _db.SaveChangesAsync();
            }
            return 0;
        }
    }
}
