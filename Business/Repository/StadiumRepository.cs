﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Business.Repository.IRepository;
using DataAccess.Data;
using DataAccess.Data.MatchData;
using DataAccess.Data.StadiumData;
using Microsoft.EntityFrameworkCore;
using Models.MatchData;
using Models.StadiumData;

namespace Business.Repository
{
    public class StadiumRepository : IStadiumRepository
    {
        private readonly ApplicationDbContext _db;

        private readonly IMapper _mapper;

        public StadiumRepository(ApplicationDbContext db, IMapper mapper)
        {
            _mapper = mapper;
            _db = db;
        }
        public async Task<IEnumerable<StadiumDTO>> GetAll()
        {
            try
            {
                IEnumerable<StadiumDTO> stadiumDTOs = _mapper.Map<IEnumerable<Stadium>, IEnumerable<StadiumDTO>>(_db.Stadiums);
                return stadiumDTOs;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<StadiumDTO> Get(int stadiumId)
        {
            try
            {
                StadiumDTO season = _mapper.Map<Stadium, StadiumDTO>(
                await _db.Stadiums.SingleOrDefaultAsync(x => x.Id == stadiumId));


                if (season != null)
                {
                    return season;
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }


        public async Task<StadiumDTO> Create(StadiumDTO stadiumDTO)
        { 
            Stadium stadium = _mapper.Map<StadiumDTO, Stadium>(stadiumDTO);
            var addedStadiums = await _db.Stadiums.AddAsync(stadium);
            await _db.SaveChangesAsync();
            return _mapper.Map<Stadium, StadiumDTO>(addedStadiums.Entity);
        }

        public async Task<StadiumDTO> Update(StadiumDTO stadiumDTO, int stadiumId)
        {
            try
            {
                if (stadiumId == stadiumDTO.Id)
                {
                    Stadium stadiumDetails = await _db.Stadiums.FindAsync(stadiumId);
                    Stadium stadium = _mapper.Map<StadiumDTO, Stadium>(stadiumDTO, stadiumDetails);
                    var updatedStadium = _db.Stadiums.Update(stadium);
                    await _db.SaveChangesAsync();
                    return _mapper.Map<Stadium, StadiumDTO>(updatedStadium.Entity);
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<int> Delete(int stadiumId)
        {
            var stadiumDetails = await _db.Stadiums.FindAsync(stadiumId);

            IEnumerable<MatchCoreDTO> matches =
                 _mapper.Map<IEnumerable<MatchCore>, IEnumerable<MatchCoreDTO>>(
                    _db.Matches
                    .Include(x => x.Stadium)
                    .Where(x => x.StadiumId == stadiumId));

            if (stadiumDetails != null && matches.Count() == 0) // if stadium exists and isn't part of any match -> delete it
            {
                _db.Stadiums.Remove(stadiumDetails);
                return await _db.SaveChangesAsync();
            }
            return 0; // return 0 if stadium is in MatchCore
        }
    }
}
