﻿using AutoMapper;
using Business.Repository.IRepository;
using DataAccess.Data;
using DataAccess.Data.CompetitionData;
using Microsoft.EntityFrameworkCore;
using Models.CompetitionData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Repository
{
    public class CompetitionRepository : ICompetitionRepository
    {
        private readonly ApplicationDbContext _db;

        private readonly IMapper _mapper;

        public CompetitionRepository(ApplicationDbContext db, IMapper mapper)
        {
            _mapper = mapper;
            _db = db;
        }
        public async Task<IEnumerable<CompetitionDTO>> GetAllFromCompParent(int competitionParentId)
        {
            try
            {
                IEnumerable<CompetitionDTO> competitions =
                _mapper.Map<IEnumerable<Competition>, IEnumerable<CompetitionDTO>>(
                    _db.Competitions
                    .Include(x => x.CompetitionParent)
                    .ThenInclude(x => x.Season)
                    .ThenInclude(x => x.Sport)
                    .Where(x => x.CompetitionParentId == competitionParentId));
                return competitions;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public async Task<IEnumerable<CompetitionDTO>> GetAllFromSport(int sportId)
        {
            try
            {
                IEnumerable<CompetitionDTO> competitions =
                _mapper.Map<IEnumerable<Competition>, IEnumerable<CompetitionDTO>>(
                    _db.Competitions
                    .Include(x => x.CompetitionParent)
                    .ThenInclude(x => x.Season)
                    .ThenInclude(x => x.Sport)
                    .Where(x => x.CompetitionParent.Season.SportId == sportId));
                return competitions;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public async Task<CompetitionDTO> Get(int competitionId)
        {
            try
            {
                CompetitionDTO competition = _mapper.Map<Competition, CompetitionDTO>(
                await _db.Competitions
                    .Include(x => x.CompetitionParent)
                    .ThenInclude(x => x.Season)
                    .ThenInclude(x => x.Sport)
                    .SingleOrDefaultAsync(x => x.Id == competitionId));


                if (competition != null)
                {
                    return competition;
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public async Task<CompetitionDTO> Create(CompetitionDTO competitionDTO)
        {
            Competition competition = _mapper.Map<CompetitionDTO, Competition>(competitionDTO);
            var addedCompetitions = await _db.Competitions.AddAsync(competition);
            await _db.SaveChangesAsync();
            return _mapper.Map<Competition, CompetitionDTO>(addedCompetitions.Entity);
        }

        public async Task<CompetitionDTO> Update(CompetitionDTO competitionDTO, int competitionId)
        {
            try
            {
                if (competitionId == competitionDTO.Id)
                {
                    Competition competitionDetails = await _db.Competitions.FindAsync(competitionId);
                    Competition competition = _mapper.Map<CompetitionDTO, Competition>(competitionDTO, competitionDetails);
                    var updatedCompetition = _db.Competitions.Update(competition);
                    await _db.SaveChangesAsync();
                    return _mapper.Map<Competition, CompetitionDTO>(updatedCompetition.Entity);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }  
        }

        public async Task<int> Delete(int competitionId)
        {
            var competitionDetails = await _db.Competitions.FindAsync(competitionId);
            if (competitionDetails != null)
            {
                _db.Competitions.Remove(competitionDetails);
                return await _db.SaveChangesAsync();
            }
            return 0;
        }

    }
}
