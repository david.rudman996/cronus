﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Business.Repository.IRepository;
using DataAccess.Data;
using DataAccess.Data.RefereeData;
using Microsoft.EntityFrameworkCore;
using Models.RefereeData;

namespace Business.Repository
{
    public class RefereeRepository : IRefereeRepository
    {
        private readonly ApplicationDbContext _db;

        private readonly IMapper _mapper;

        public RefereeRepository(ApplicationDbContext db, IMapper mapper)
        {
            _mapper = mapper;
            _db = db;
        }

        public async Task<IEnumerable<RefereeDTO>> GetAll()
        {
            try
            {
                IEnumerable<RefereeDTO> refereeDTOs = _mapper.Map<IEnumerable<Referee>, IEnumerable<RefereeDTO>>(_db.Referees);
                return refereeDTOs;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<RefereeDTO> Get(int refereeId)
        {
            try
            {
                RefereeDTO referee = _mapper.Map<Referee, RefereeDTO>(
                await _db.Referees.SingleOrDefaultAsync(x => x.Id == refereeId));


                if (referee != null)
                {
                    return referee;
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<RefereeDTO> Create(RefereeDTO refereeDTO)
        {
            Referee referee = _mapper.Map<RefereeDTO, Referee>(refereeDTO);
            var addedReferees = await _db.Referees.AddAsync(referee);
            await _db.SaveChangesAsync();
            return _mapper.Map<Referee, RefereeDTO>(addedReferees.Entity);
        }

        public async Task<RefereeDTO> Update(RefereeDTO refereeDTO, int refereeId)
        {
            try
            {
                if (refereeId == refereeDTO.Id)
                {
                    Referee refereeDetails = await _db.Referees.FindAsync(refereeId);
                    Referee referee = _mapper.Map<RefereeDTO, Referee>(refereeDTO, refereeDetails);
                    var updatedReferee = _db.Referees.Update(referee);
                    await _db.SaveChangesAsync();
                    return _mapper.Map<Referee, RefereeDTO>(updatedReferee.Entity);
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<int> Delete(int refereeId)
        {
            var refereeDetails = await _db.Referees.FindAsync(refereeId);
            IEnumerable<RefereeMatchDTO> refereeMatch = _mapper.Map<IEnumerable<RefereeMatch>, IEnumerable<RefereeMatchDTO>>(
                _db.RefereeMatches
                .Include(x => x.Referee)
                .Where(x => x.RefereeId == refereeId));
            if (refereeDetails != null && refereeMatch.Count() == 0) //if referee exists and isn't part of any match -> delete it
            {
                _db.Referees.Remove(refereeDetails);
                return await _db.SaveChangesAsync();
            }
            return 0; // return 0 if stadium is in MatchCore
        }
    }
}
