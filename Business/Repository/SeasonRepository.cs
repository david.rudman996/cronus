﻿using AutoMapper;
using Business.Repository.IRepository;
using DataAccess.Data;
using DataAccess.Data.CompetitionData;
using DataAccess.Data.SeasonData;
using Microsoft.EntityFrameworkCore;
using Models.CompetitionData;
using Models.SeasonData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Repository
{
    public class SeasonRepository : ISeasonRepository
    {
        private readonly ApplicationDbContext _db;

        private readonly IMapper _mapper;

        public SeasonRepository(ApplicationDbContext db, IMapper mapper)
        {
            _mapper = mapper;
            _db = db;
        }
        public async Task<IEnumerable<SeasonDTO>> GetAllFromSport(int sportId)
        {
            try
            {
                IEnumerable<SeasonDTO> seasonDTOs =
                    _mapper.Map<IEnumerable<Season>, IEnumerable<SeasonDTO>>(
                    _db.Seasons.Include(x => x.Sport)
                    .Where(x => x.Sport.Id == sportId));
                return seasonDTOs;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public async Task<SeasonDTO> Get(int seasonId)
        {
            try
            {
                SeasonDTO season = _mapper.Map<Season, SeasonDTO>(
                await _db.Seasons
                    .Include(x => x.Sport)
                    .SingleOrDefaultAsync(x => x.Id == seasonId));


                if (season != null)
                {
                    return season;
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<SeasonDTO> Create(SeasonDTO seasonDTO)
        {
            Season season = _mapper.Map<SeasonDTO, Season>(seasonDTO);
            var addedSeasons = await _db.Seasons.AddAsync(season);
            await _db.SaveChangesAsync();
            return _mapper.Map<Season, SeasonDTO>(addedSeasons.Entity);
        }

        public async Task<SeasonDTO> Update(SeasonDTO seasonDTO, int seasonId)
        {
            try
            {
                if (seasonId == seasonDTO.Id)
                {
                    Season seasonDetails = await _db.Seasons.FindAsync(seasonId);
                    Season season = _mapper.Map<SeasonDTO, Season>(seasonDTO, seasonDetails);
                    var updatedSeason = _db.Seasons.Update(season);
                    await _db.SaveChangesAsync();
                    return _mapper.Map<Season, SeasonDTO>(updatedSeason.Entity);
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<int> Delete(int seasonId)
        {
            {
                var seasonDetails = await _db.Seasons.FindAsync(seasonId);
                IEnumerable<CompetitionParentDTO> competitionParents =
                _mapper.Map<IEnumerable<CompetitionParent>, IEnumerable<CompetitionParentDTO>>(
                    _db.CompetitionParents
                    .Include(x => x.Season)
                    .Where(x => x.SeasonId == seasonId));
                if (seasonDetails.IsActive == true)
                {
                    return 5; //if season is active, you cannot delete it
                }

                if (seasonDetails != null && competitionParents.Count() == 0)
                {
                    _db.Seasons.Remove(seasonDetails);
                    return await _db.SaveChangesAsync();
                }
                return 0; // if season is a part of CompetitionParent
            }
        }

        public async Task<SeasonDTO> SetActiveSeason(int seasonId, int sportId)
        {
            Season activeSeason =  await _db.Seasons.SingleOrDefaultAsync(x => x.IsActive == true && x.Sport.Id == sportId); //find active season in sport
            if(activeSeason != null) //if active season already exists
            {
                // set "activeSeason" IsActive to false
                activeSeason.IsActive = false;
                var seasonActiveUpdated = _db.Seasons.Update(activeSeason);
            }

            Season seasonDetails = await _db.Seasons.FindAsync(seasonId); //find season we need to set as active
            seasonDetails.IsActive = true; //set IsActive to true --> should be the only season in that sport thats active

            var updatedSeason = _db.Seasons.Update(seasonDetails); //update database
            await _db.SaveChangesAsync();
            return _mapper.Map<Season, SeasonDTO>(updatedSeason.Entity);

        }
    }
}
