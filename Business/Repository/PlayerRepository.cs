﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Business.Repository.IRepository;
using DataAccess.Data;
using DataAccess.Data.PlayerData;
using DataAccess.Data.TeamData;
using Microsoft.EntityFrameworkCore;
using Models.PlayerData;
using Models.TeamData;

namespace Business.Repository
{
    public class PlayerRepository : IPlayerRepository
    {
        private readonly ApplicationDbContext _db;

        private readonly IMapper _mapper;

        public PlayerRepository(ApplicationDbContext db, IMapper mapper)
        {
            _mapper = mapper;
            _db = db;
        }
        public async Task<IEnumerable<PlayerDTO>> GetAllFromSport(int sportId)
        {
            try
            {
                IEnumerable<PlayerDTO> players =
                _mapper.Map<IEnumerable<Player>, IEnumerable<PlayerDTO>>(
                    _db.Players.FromSqlRaw("SELECT * FROM dbo.Players WHERE SportId = {0}", sportId)
                    .Include(x => x.Sport).ToList());
                return players;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public async Task<IEnumerable<PlayerDTO>> GetAllAvailablePlayers(int sportId, int seasonId)
        {
            try
            {
                IEnumerable<PlayerDTO> allPlayers =
                _mapper.Map<IEnumerable<Player>, IEnumerable<PlayerDTO>>(
                    _db.Players
                    .Include(x => x.Sport)
                    .Where(x => x.SportId == sportId));

                IEnumerable<TeamPlayerDTO> teamPlayers = _mapper.Map<IEnumerable<TeamPlayer>, IEnumerable<TeamPlayerDTO>>(
                    _db.TeamPlayers
                    .Include(x => x.Player).ThenInclude(x => x.Sport)
                    .Include(x => x.TeamSeason).ThenInclude(x => x.Season)
                    .Where(x => x.Player.SportId == sportId && x.TeamSeason.SeasonId == seasonId));

                var available = allPlayers.Where(x1 => teamPlayers.All(x2 => x2.Player.Id != x1.Id)).ToList();

                return available;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public async Task<PlayerDTO> Get(int playerId)
        {
            try
            {
                PlayerDTO player = _mapper.Map<Player, PlayerDTO>(
                await _db.Players.Include(x => x.Sport).SingleOrDefaultAsync(x => x.Id == playerId));

                if (player != null)
                {
                    return player;
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<PlayerDTO> Create(PlayerDTO playerDTO)
        {
            Player player = _mapper.Map<PlayerDTO, Player>(playerDTO);
            var addedPlayers = await _db.Players.AddAsync(player);
            await _db.SaveChangesAsync();
            return _mapper.Map<Player, PlayerDTO>(addedPlayers.Entity);
        }

        public async Task<PlayerDTO> Update(PlayerDTO playerDTO, int playerId)
        {
            try
            {
                if (playerId == playerDTO.Id)
                {
                    Player playerDetails = await _db.Players.FindAsync(playerId);
                    Player player = _mapper.Map<PlayerDTO, Player>(playerDTO, playerDetails);
                    var updatedPlayer = _db.Players.Update(player);
                    await _db.SaveChangesAsync();
                    return _mapper.Map<Player, PlayerDTO>(updatedPlayer.Entity);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<int> Delete(int playerId)
        {
            var playerDetails = await _db.Players.FindAsync(playerId);
            if (playerDetails != null)
            {
                _db.Players.Remove(playerDetails);
                return await _db.SaveChangesAsync();
            }
            return 0;
        }
    }
}
