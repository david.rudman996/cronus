﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Business.Repository.IRepository;
using DataAccess.Data;
using DataAccess.Data.TeamData;
using Microsoft.EntityFrameworkCore;
using Models.TeamData;

namespace Business.Repository
{
    public class TeamRepository : ITeamRepository
    {
        private readonly ApplicationDbContext _db;

        private readonly IMapper _mapper;

        public TeamRepository(ApplicationDbContext db, IMapper mapper)
        {
            _mapper = mapper;
            _db = db;
        }

        public async Task<IEnumerable<TeamDTO>> GetAll()
        {
            try
            {
                IEnumerable<TeamDTO> teamDTOs = _mapper.Map<IEnumerable<Team>, IEnumerable<TeamDTO>>(_db.Teams);
                return teamDTOs;
            }
            catch (Exception)
            {
                return null;
            }
        }


        public async Task<IEnumerable<TeamDTO>> GetAllFromSport(int sportId)
        {
            try
            {
                IEnumerable<TeamDTO> teams =
                    _mapper.Map<IEnumerable<Team>, IEnumerable<TeamDTO>>(
                    _db.Teams.Include(x => x.Sport).Where(x => x.Sport.Id == sportId));
                return teams;
            }
            catch (Exception)
            {
                return null;
            }

        }

        public async Task<IEnumerable<TeamDTO>> GetAllAvailableTeamsForTeamSeason(int seasonId, int sportId)
        {
            IEnumerable<TeamSeasonDTO> teamSeasons =
                    _mapper.Map<IEnumerable<TeamSeason>, IEnumerable<TeamSeasonDTO>>(
                        _db.TeamSeasons.
                        Include(x => x.Season)
                        .ThenInclude(x => x.Sport)
                        .Include(x => x.Team)
                        .Where(x => x.Season.Id == seasonId));
            IEnumerable<TeamDTO> teams =
                    _mapper.Map<IEnumerable<Team>, IEnumerable<TeamDTO>>(
                    _db.Teams.Include(x => x.Sport).Where(x => x.Sport.Id == sportId));
            var available = teams.Where(x1 => teamSeasons.All(x2 => x2.TeamId != x1.Id)).ToList();

            return available;
        }

        public async Task<TeamDTO> Get(int teamId)
        {
            {
                try
                {
                    TeamDTO team = _mapper.Map<Team, TeamDTO>(
                    await _db.Teams.Include(x => x.Sport).SingleOrDefaultAsync(x => x.Id == teamId));

                    if (team != null)
                    {
                        return team;
                    }
                    return null;
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public async Task<TeamDTO> Create(TeamDTO teamDTO)
        {
            Team team = _mapper.Map<TeamDTO, Team>(teamDTO);
            var addedTeams = await _db.Teams.AddAsync(team);
            await _db.SaveChangesAsync();
            return _mapper.Map<Team, TeamDTO>(addedTeams.Entity);
        }

        public async Task<TeamDTO> Update(TeamDTO teamDTO, int teamId)
        {
            try
            {
                if (teamId == teamDTO.Id)
                {
                    Team teamDetails = await _db.Teams.FindAsync(teamId);
                    Team team = _mapper.Map<TeamDTO, Team>(teamDTO, teamDetails);
                    var updatedTeam = _db.Teams.Update(team);
                    await _db.SaveChangesAsync();
                    return _mapper.Map<Team, TeamDTO>(updatedTeam.Entity);
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<int> Delete(int teamId)
        {
            var teamDetails = await _db.Teams.FindAsync(teamId);
            if (teamDetails != null)
            {
                _db.Teams.Remove(teamDetails);
                return await _db.SaveChangesAsync();
            }
            return 0;
        }
    }
}
