﻿using AutoMapper;
using Business.Repository.IRepository;
using DataAccess.Data;
using DataAccess.Data.MatchData;
using DataAccess.Data.RoundData;
using Microsoft.EntityFrameworkCore;
using Models.RoundData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Repository
{
    public class RoundRepository : IRoundRepository
    {
        private readonly ApplicationDbContext _db;

        private readonly IMapper _mapper;

        public RoundRepository(ApplicationDbContext db, IMapper mapper)
        {
            _mapper = mapper;
            _db = db;
        }
        public async Task<IEnumerable<RoundDTO>> GetAllFromCompetition(int competitionId)
        {  
            try
            {
                IEnumerable<RoundDTO> rounds =
                _mapper.Map<IEnumerable<Round>, IEnumerable<RoundDTO>>(
                    _db.Rounds.Where(x => x.Competition.Id == competitionId));
                return rounds;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                return null;
            }
        }
        public async Task<IEnumerable<RoundDTO>> GetAllFromSport(int sportId)
        {
            try
            {
                IEnumerable<RoundDTO> rounds =
                _mapper.Map<IEnumerable<Round>, IEnumerable<RoundDTO>>(
                    _db.Rounds.Where(x => x.Competition.CompetitionParent.Season.Sport.Id == sportId));
                return rounds;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                return null;
            }
        }

        public async Task<RoundDTO> Get(int roundId)
        {
            try
            {
                RoundDTO round = _mapper.Map<Round, RoundDTO>(
                await _db.Rounds.SingleOrDefaultAsync(x => x.Id == roundId));

                if (round != null)
                {
                    return round;
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<RoundDTO> Create(RoundDTO roundDTO)
        {
            Round round = _mapper.Map<RoundDTO, Round>(roundDTO);
            var addedMatches = await _db.Rounds.AddAsync(round);
            await _db.SaveChangesAsync();
            return _mapper.Map<Round, RoundDTO>(addedMatches.Entity);
        }
        public async Task<RoundDTO> Update(RoundDTO roundDTO, int roundId)
        {
            try
            {
                if (roundId == roundDTO.Id)
                {
                    Round roundDetails = await _db.Rounds.FindAsync(roundId);
                    Round round = _mapper.Map<RoundDTO, Round>(roundDTO, roundDetails);
                    var updatedRound = _db.Rounds.Update(roundDetails);
                    await _db.SaveChangesAsync();
                    return _mapper.Map<Round, RoundDTO>(updatedRound.Entity);
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<int> Delete(int roundId)
        {
            var roundDetails = await _db.Rounds.FindAsync(roundId);
            if (roundDetails != null)
            {
                _db.Rounds.Remove(roundDetails);
                return await _db.SaveChangesAsync();
            }
            return 0;
        }
    }
}
