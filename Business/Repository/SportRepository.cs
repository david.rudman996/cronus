﻿using AutoMapper;
using Business.Repository.IRepository;
using DataAccess.Data;
using DataAccess.Data.SportData;
using Microsoft.EntityFrameworkCore;
using Models.SportData;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Business.Repository
{
    public class SportRepository : ISportRepository
    {
        private readonly ApplicationDbContext _db;

        private readonly IMapper _mapper;

        public SportRepository(ApplicationDbContext db, IMapper mapper)
        {
            _mapper = mapper;
            _db = db;
        }
        public async Task<IEnumerable<SportDTO>> GetAll()
        {
            try
            {
                IEnumerable<SportDTO> sportDTOs =  _mapper.Map<IEnumerable<Sport>, IEnumerable<SportDTO>>(_db.Sports);
                return sportDTOs;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public async Task<SportDTO> Get(int sportId)
        {
            try
            {
                SportDTO sport = _mapper.Map<Sport, SportDTO>(
                    await _db.Sports.SingleOrDefaultAsync(x => x.Id == sportId));

                    return sport;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<SportDTO> GetByName(string name)
        {
            try
            {
                SportDTO sport = _mapper.Map<Sport, SportDTO>(
                    await _db.Sports.SingleOrDefaultAsync(x => x.Name == name));

                return sport;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
