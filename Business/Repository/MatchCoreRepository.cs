﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Business.Repository.IRepository;
using DataAccess.Data;
using DataAccess.Data.MatchData;
using Microsoft.EntityFrameworkCore;
using Models.MatchData;

namespace Business.Repository
{
    public class MatchCoreRepository : IMatchCoreRepository
    {
        private readonly ApplicationDbContext _db;

        private readonly IMapper _mapper;

        public MatchCoreRepository(ApplicationDbContext db, IMapper mapper)
        {
            _mapper = mapper;
            _db = db;
        }
        public async Task<IEnumerable<MatchCoreDTO>> GetAllMatchesFromSport(int sportId, int seasonId)
        {
            try
            {
                IEnumerable<MatchCoreDTO> matches =
                 _mapper.Map<IEnumerable<MatchCore>, IEnumerable<MatchCoreDTO>>(
                    _db.Matches
                    .Include(x => x.Stadium)
                    .Include(x => x.Competition).ThenInclude(x => x.CompetitionParent).ThenInclude(x => x.Season)
                    .Include(x => x.TeamHome).ThenInclude(x => x.Sport)
                    .Include(x => x.TeamAway)
                    .Include(x => x.Round)
                    .Include(x => x.PlayerOfTheMatch)
                    .Include(x => x.MatchStatus)
                    .Where(x => x.TeamHome.Sport.Id == sportId)
                    .Where(x => x.Competition.CompetitionParent.Season.Id == seasonId));
                return matches;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public async Task<MatchCoreDTO> Get(int matchCoreId)
        {
            try
            {
                MatchCoreDTO match = _mapper.Map<MatchCore, MatchCoreDTO>(
                await _db.Matches
                    .Include(x => x.TeamHome).ThenInclude(x => x.Sport)
                    .Include(x => x.TeamAway)
                    .Include(x => x.Stadium)
                    .Include(x => x.PlayerOfTheMatch)
                    .Include(x => x.MatchStatus)
                    .Include(x => x.Round)
                    .Include(x => x.Competition)
                    .ThenInclude(x => x.CompetitionParent)
                    .ThenInclude(x => x.Season).SingleOrDefaultAsync(x => x.Id == matchCoreId));
                if (match != null)
                {
                    return match;
                }
                return null;
            }
            catch (Exception ex) 
            {
                throw ex;
            }
        }

        public int Create(MatchCoreDTO matchCoreDTO)
        {
            try
            {
                MatchCore match = _mapper.Map<MatchCoreDTO, MatchCore>(matchCoreDTO);
                var addedMatches = _db.Matches.Add(match);
                _db.SaveChanges();
                return match.Id;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public async Task<MatchCoreDTO> Update(MatchCoreDTO matchCoreDTO, int matchCoreId)
        {
            try
            {
                if (matchCoreId == matchCoreDTO.Id)
                {
                    MatchCore matchtDetails = await _db.Matches.FindAsync(matchCoreId);
                    MatchCore match = _mapper.Map<MatchCoreDTO, MatchCore>(matchCoreDTO, matchtDetails);
                    var updatedMatch = _db.Matches.Update(match);
                    await _db.SaveChangesAsync();
                    return _mapper.Map<MatchCore, MatchCoreDTO>(updatedMatch.Entity);
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<int> Delete(int matchCoreId)
        {
            var matchDetails = await _db.Matches.FindAsync(matchCoreId);
            if (matchDetails != null)
            {
                _db.Matches.Remove(matchDetails);
                return await _db.SaveChangesAsync();
            }
            return 0;
        }
    }
}
