﻿using AutoMapper;
using Business.Repository.IRepository;
using DataAccess.Data;
using DataAccess.Data.CompetitionData;
using DataAccess.Data.SeasonData;
using Microsoft.EntityFrameworkCore;
using Models.CompetitionData;
using Models.SeasonData;
using Models.SportData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Repository
{
    public class CompetitionParentRepository : ICompetitionParentRepository
    {
        private readonly ApplicationDbContext _db;

        private readonly IMapper _mapper;

        public CompetitionParentRepository(ApplicationDbContext db, IMapper mapper)
        {
            _mapper = mapper;
            _db = db;
        }
        public async Task<IEnumerable<CompetitionParentDTO>> GetAllFromSeason(int seasonId)
        {
            try
            {
                IEnumerable<CompetitionParentDTO> competitionParents =
                _mapper.Map<IEnumerable<CompetitionParent>, IEnumerable<CompetitionParentDTO>>(
                    _db.CompetitionParents.Include(x => x.Season).ThenInclude(x => x.Sport).Where(x => x.Season.Id == seasonId));
                return competitionParents;
            }
            catch(Exception)
            {
                return null;
            }

        }
        public async Task<CompetitionParentDTO> Get(int competitionParentId)
        {
            try
            {
                CompetitionParentDTO competitionParent = _mapper.Map<CompetitionParent, CompetitionParentDTO>(
                await _db.CompetitionParents.Include(x => x.Season).ThenInclude(x => x.Sport).SingleOrDefaultAsync(x => x.Id == competitionParentId));


                if (competitionParent != null)
                {
                    return competitionParent;
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }            
        }

        public async Task<CompetitionParentDTO> Create(CompetitionParentDTO competitionParentDTO)
        {
            CompetitionParent competitionParent = _mapper.Map<CompetitionParentDTO, CompetitionParent>(competitionParentDTO);
            var addedCompetitions = await _db.CompetitionParents.AddAsync(competitionParent);
            await _db.SaveChangesAsync();
            return _mapper.Map<CompetitionParent, CompetitionParentDTO>(addedCompetitions.Entity);
        }

        public async Task<CompetitionParentDTO> Update(CompetitionParentDTO competitionParentDTO, int competitionParentId)
        {
            try
            {
                if(competitionParentId == competitionParentDTO.Id)
                {
                    CompetitionParent competitionDetails = await _db.CompetitionParents.FindAsync(competitionParentId);
                    CompetitionParent competition = _mapper.Map<CompetitionParentDTO, CompetitionParent>(competitionParentDTO, competitionDetails);
                    var updatedCompetitionParent = _db.CompetitionParents.Update(competition);
                    await _db.SaveChangesAsync();
                    return _mapper.Map<CompetitionParent, CompetitionParentDTO>(updatedCompetitionParent.Entity);
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<int> Delete(int competitionParentId)
        {
            var competitionDetails = await _db.CompetitionParents.FindAsync(competitionParentId);
            if (competitionDetails != null)
            {
                _db.CompetitionParents.Remove(competitionDetails);
                return await _db.SaveChangesAsync();
            }
            return 0;
        }       
    }
}
