﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Business.Repository.IRepository;
using DataAccess.Data;
using DataAccess.Data.MatchData;
using Microsoft.EntityFrameworkCore;
using Models.MatchData;

namespace Business.Repository
{
    public class MatchCommentaryRepository : IMatchCommentaryRepository
    {
        private readonly ApplicationDbContext _db;

        private readonly IMapper _mapper;

        public MatchCommentaryRepository(ApplicationDbContext db, IMapper mapper)
        {
            _mapper = mapper;
            _db = db;
        }

        public async Task<IEnumerable<MatchCommentaryDTO>> GetAllForMatch(int matchId)
        {
            try
            {
                IEnumerable<MatchCommentaryDTO> matchCommentary =
                    _mapper.Map<IEnumerable<MatchCommentary>, IEnumerable<MatchCommentaryDTO>>(
                        _db.MatchCommentaries
                        .Where(x => x.Match.Id == matchId));
                return matchCommentary;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<MatchCommentaryDTO> Get(int matchCommentaryId)
        {
            try
            {
                MatchCommentaryDTO matchCommentary = _mapper.Map<MatchCommentary, MatchCommentaryDTO>(
                    await _db.MatchCommentaries.SingleOrDefaultAsync(x => x.Id == matchCommentaryId));
                return matchCommentary;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<MatchCommentaryDTO> Create(MatchCommentaryDTO matchCommentaryDTO)
        {
            MatchCommentary matchCommentary = _mapper.Map<MatchCommentaryDTO, MatchCommentary>(matchCommentaryDTO);
            var addedComments = await _db.MatchCommentaries.AddAsync(matchCommentary);
            await _db.SaveChangesAsync();
            return _mapper.Map<MatchCommentary, MatchCommentaryDTO>(addedComments.Entity);
        }

        public async Task<MatchCommentaryDTO> Update(MatchCommentaryDTO matchCommentaryDTO, int matchCommentaryId)
        {
            try
            {
                if (matchCommentaryId == matchCommentaryDTO.Id)
                {
                    MatchCommentary matchCommentDetails = await _db.MatchCommentaries.FindAsync(matchCommentaryId);
                    MatchCommentary matchComment = _mapper.Map<MatchCommentaryDTO, MatchCommentary>(matchCommentaryDTO, matchCommentDetails);
                    var updatedMatchComment = _db.MatchCommentaries.Update(matchComment);
                    await _db.SaveChangesAsync();
                    return _mapper.Map<MatchCommentary, MatchCommentaryDTO>(updatedMatchComment.Entity);
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<int> Delete(int matchCommentaryId)
        {
            var matchCommentDetails = await _db.MatchCommentaries.FindAsync(matchCommentaryId);
            if (matchCommentDetails != null)
            {
                _db.MatchCommentaries.Remove(matchCommentDetails);
                return await _db.SaveChangesAsync();
            }
            return 0;
        }

    }
}
