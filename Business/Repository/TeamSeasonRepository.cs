﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Business.Repository.IRepository;
using DataAccess.Data;
using DataAccess.Data.TeamData;
using DataAccess.Data.SeasonData;
using Microsoft.EntityFrameworkCore;
using Models.SeasonData;
using Models.TeamData;

namespace Business.Repository
{
    public class TeamSeasonRepository : ITeamSeasonRepository
    {
        private readonly ApplicationDbContext _db;

        private readonly IMapper _mapper;

        public TeamSeasonRepository(ApplicationDbContext db, IMapper mapper)
        {
            _mapper = mapper;
            _db = db;
        }
        public async Task<IEnumerable<TeamSeasonDTO>> GetAllFromSeason(int seasonId)
        {
            try
            {
                IEnumerable<TeamSeasonDTO> teamSeasons =
                    _mapper.Map<IEnumerable<TeamSeason>, IEnumerable<TeamSeasonDTO>>(
                        _db.TeamSeasons.
                        Include(x => x.Season)
                        .ThenInclude(x => x.Sport)
                        .Include(x => x.Team)
                        .Where(x => x.Season.Id == seasonId).OrderBy(x => x.Team.Name));
                return teamSeasons;
            }
            catch (Exception)
            {
                return null;
            }

        }

        public async Task<TeamSeasonDTO> Get(int teamSeasonId)
        {
            try
            {
                TeamSeasonDTO teamSeason = _mapper.Map<TeamSeason, TeamSeasonDTO>(
                await _db.TeamSeasons
                    .Include(x => x.Season)
                    .ThenInclude(x => x.Sport)
                    .Include(x => x.Team)
                    .SingleOrDefaultAsync(x => x.Id == teamSeasonId));

                if (teamSeason != null)
                {
                    return teamSeason;
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<TeamSeasonDTO> Create(TeamSeasonDTO teamSeasonDTO)
        {
            TeamSeason teamSeason = _mapper.Map<TeamSeasonDTO, TeamSeason>(teamSeasonDTO);
            //Season seasonDetails = _mapper.Map<Season, SeasonDTO>(await _db.Seasons.SingleOrDefaultAsync(x => x.Id == teamSeasonDTO.SeasonId);
            var addedTeamSeasons = await _db.TeamSeasons.AddAsync(teamSeason);
            await _db.SaveChangesAsync();
            return _mapper.Map<TeamSeason, TeamSeasonDTO>(addedTeamSeasons.Entity);
        }

        public async Task<TeamSeasonDTO> Update(TeamSeasonDTO teamSeasonDTO, int teamSeasonId)
        {
            try
            {
                if (teamSeasonId == teamSeasonDTO.Id)
                {
                    TeamSeason teamSeasonDetails = await _db.TeamSeasons.FindAsync(teamSeasonId);
                    TeamSeason teamSeason = _mapper.Map<TeamSeasonDTO, TeamSeason>(teamSeasonDTO, teamSeasonDetails);
                    var updatedTeamSeason = _db.TeamSeasons.Update(teamSeason);
                    await _db.SaveChangesAsync();
                    return _mapper.Map<TeamSeason, TeamSeasonDTO>(updatedTeamSeason.Entity);
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<int> Delete(int teamSeasonId)
        {
            var teamSeasonDetails = await _db.TeamSeasons.FindAsync(teamSeasonId);
            if (teamSeasonDetails != null)
            {
                _db.TeamSeasons.Remove(teamSeasonDetails);
                return await _db.SaveChangesAsync();
            }
            return 0;
        }
    }
}
