﻿using Models.MatchData;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Business.Repository.IRepository
{
    public interface IMatchFutsalRepository
    {
        public Task<IEnumerable<MatchFutsalDTO>> GetAll();
        public Task<MatchFutsalDTO> GetFromMatch(int matchCoreId);
        public Task<MatchFutsalDTO> Get(int matchId);
        public Task<MatchFutsalDTO> Create(MatchFutsalDTO matchFutsalDTO);
        public Task<MatchFutsalDTO> Update(MatchFutsalDTO matchFutsalDTO, int matchFutsalId);
        public Task<MatchFutsalDTO> UpdateMinutesAndStatus(MatchFutsalDTO matchFutsalDTO, int matchFutsalId);
        public Task<int> Delete(int matchFutsalId);
    }
}
