﻿using Models.CompetitionData;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Business.Repository.IRepository
{
    public interface ICompetitionParentRepository
    {
        public Task<IEnumerable<CompetitionParentDTO>> GetAllFromSeason(int seasonId);
        public Task<CompetitionParentDTO> Get(int competitionParentId);
        public Task<CompetitionParentDTO> Create(CompetitionParentDTO competitionParentDTO);
        public Task<CompetitionParentDTO> Update(CompetitionParentDTO competitionParentDTO, int competitionParentId);
        public Task<int> Delete(int competitionParentId);
    }
}
