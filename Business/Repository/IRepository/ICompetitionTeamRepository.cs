﻿using Models.CompetitionData;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Business.Repository.IRepository
{
    public interface ICompetitionTeamRepository
    {
        public Task<IEnumerable<CompetitionTeamDTO>> GetAllFromCompetition(int competitionId);
        public Task<CompetitionTeamDTO> GetCompTeamFromTeam(int teamId);

        public Task<CompetitionTeamDTO> Create(CompetitionTeamDTO competitionTeamDTO);
        public Task<CompetitionTeamDTO> Update(CompetitionTeamDTO competitionTeamDTO, int competitionTeamId);
        public Task<int> Recalculate(int competitionId, string sportName);
        public Task<int> SetAllValuesToZero(int competitionId);
        public Task<int> Delete(int competitionTeamId);
    }
}
