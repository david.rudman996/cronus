﻿using Models.RoundData;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Repository.IRepository
{
    public interface IRoundRepository
    {
        public Task<IEnumerable<RoundDTO>> GetAllFromCompetition(int competitionId);
        public Task<IEnumerable<RoundDTO>> GetAllFromSport(int sportId);
        public Task<RoundDTO> Get(int roundId);
        public Task<RoundDTO> Create(RoundDTO roundDTO);
        public Task<RoundDTO> Update(RoundDTO roundDTO, int roundId);
        public Task<int> Delete(int roundId);
    }
}
