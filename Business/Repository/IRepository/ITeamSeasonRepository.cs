﻿using Models.TeamData;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Business.Repository.IRepository
{
    public interface ITeamSeasonRepository
    {
        public Task<IEnumerable<TeamSeasonDTO>> GetAllFromSeason(int seasonId);
        public Task<TeamSeasonDTO> Get(int teamSeasonId);
        public Task<TeamSeasonDTO> Create(TeamSeasonDTO teamSeasonDTO);
        public Task<TeamSeasonDTO> Update(TeamSeasonDTO teamSeasonDTO, int teamSeasonId);
        public Task<int> Delete(int teamSeasonId);
    }
}
