﻿using DataAccess.Data;
using Models.PlayerData;
using Models.TeamData;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Business.Repository.IRepository
{
    public interface ITeamPlayerRepository
    {
        public Task<IEnumerable<TeamPlayerDTO>> GetAll();
        public Task<IEnumerable<TeamPlayerDTO>> GetAllFromTeamSeason(int teamSeasonId);
        public Task<TeamPlayerDTO> Get(int teamPlayerId);
        public Task Create(IEnumerable<PlayerDTO> teamPlayerDTOs, int teamSeasonId);
        public Task<int> Delete(int teamPlayerId);
    }
}
