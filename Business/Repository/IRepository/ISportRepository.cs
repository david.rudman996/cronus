﻿using Models.SportData;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Business.Repository.IRepository
{
    public interface ISportRepository
    {
        public Task<IEnumerable<SportDTO>> GetAll();
        public Task<SportDTO> Get(int sportId);
        public Task<SportDTO> GetByName(string name);
    }
}
