﻿using Models.TeamData;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Business.Repository.IRepository
{
    public interface ITeamRepository
    {
        public Task<IEnumerable<TeamDTO>> GetAll();
        public Task<IEnumerable<TeamDTO>> GetAllFromSport(int sportId);
        public Task<IEnumerable<TeamDTO>> GetAllAvailableTeamsForTeamSeason(int seasonId, int sportId);
        public Task<TeamDTO> Get(int teamId);
        public Task<TeamDTO> Create(TeamDTO teamDTO);
        public Task<TeamDTO> Update(TeamDTO teamDTO, int teamId);
        public Task<int> Delete(int teamId);
    }
}
