﻿using Models.CompetitionData;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Repository.IRepository
{
    public interface ICompetitionRepository
    {
        public Task<IEnumerable<CompetitionDTO>> GetAllFromCompParent(int competitionParentId);
        public Task<IEnumerable<CompetitionDTO>> GetAllFromSport(int sportId);
        public Task<CompetitionDTO> Get(int competitionId);
        public Task<CompetitionDTO> Create(CompetitionDTO competitionDTO);
        public Task<CompetitionDTO> Update(CompetitionDTO competitionDTO, int competitionId);
        public Task<int> Delete(int competitionId);
    }
}
