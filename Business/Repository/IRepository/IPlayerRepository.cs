﻿using Models.PlayerData;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Business.Repository.IRepository
{
    public interface IPlayerRepository
    {
        public Task<IEnumerable<PlayerDTO>> GetAllFromSport(int sportId);
        public Task<IEnumerable<PlayerDTO>> GetAllAvailablePlayers(int sportId, int seasonId);
        public Task<PlayerDTO> Get(int playerId);
        public Task<PlayerDTO> Create(PlayerDTO playerDTO);
        public Task<PlayerDTO> Update(PlayerDTO playerDTO, int playerId);
        public Task<int> Delete(int playerId);
    }
}
