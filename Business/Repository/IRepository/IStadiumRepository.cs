﻿using Models.StadiumData;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Business.Repository.IRepository
{
    public interface IStadiumRepository
    {
        public Task<IEnumerable<StadiumDTO>> GetAll();
        public Task<StadiumDTO> Get(int stadiumId);
        public Task<StadiumDTO> Create(StadiumDTO stadiumDTO);
        public Task<StadiumDTO> Update(StadiumDTO stadiumDTO, int stadiumId);
        public Task<int> Delete(int stadiumId);
    }
}
