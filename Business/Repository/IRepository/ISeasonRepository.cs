﻿using Models.SeasonData;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Business.Repository.IRepository
{
    public interface ISeasonRepository
    {
        public Task<IEnumerable<SeasonDTO>> GetAllFromSport(int sportId);
        public Task<SeasonDTO> Get(int seasonId);
        public Task<SeasonDTO> Create(SeasonDTO seasonDTO);
        public Task<SeasonDTO> Update(SeasonDTO seasonDTO, int seasonId);
        public Task<int> Delete(int seasonId);
        public Task<SeasonDTO> SetActiveSeason(int seasonId, int sportId);
    }
}
