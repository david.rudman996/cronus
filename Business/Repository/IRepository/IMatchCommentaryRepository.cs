﻿using Models.MatchData;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Business.Repository.IRepository
{
    public interface IMatchCommentaryRepository
    {
        public Task<IEnumerable<MatchCommentaryDTO>> GetAllForMatch(int matchId);
        public Task<MatchCommentaryDTO> Get(int matchCommentaryId);
        public Task<MatchCommentaryDTO> Create(MatchCommentaryDTO matchCommentaryDTO);
        public Task<MatchCommentaryDTO> Update(MatchCommentaryDTO matchCommentaryDTO, int matchCommentaryId);
        public Task<int> Delete(int matchCommentaryId);
    }
}
