﻿using Models.RefereeData;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Business.Repository.IRepository
{
    public interface IRefereeMatchRepository
    {
        public Task<RefereeMatchDTO> Get(int matchId);
        public Task<RefereeMatchDTO> GetFromMatch(int matchCoreId);
        public Task<RefereeMatchDTO> Create(RefereeMatchDTO refereeMatchDTO);
        public Task<RefereeMatchDTO> Update(RefereeMatchDTO refereeMatchDTO, int refereeMatchId);
        public Task<int> Delete(int refereeMatchId);
    }
}
