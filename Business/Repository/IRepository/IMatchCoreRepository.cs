﻿using DataAccess.Data.MatchData;
using Models.MatchData;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Business.Repository.IRepository
{
    public interface IMatchCoreRepository
    {
        public Task<IEnumerable<MatchCoreDTO>> GetAllMatchesFromSport(int sportId, int seasonId); 
        public Task<MatchCoreDTO> Get(int matchCoreId);
        public int Create(MatchCoreDTO matchCoreDTO);
        public Task<MatchCoreDTO> Update(MatchCoreDTO matchCoreDTO, int matchCoreId);
        public Task<int> Delete(int matchCoreId);
    }
}
