﻿using Models.RefereeData;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Business.Repository.IRepository
{
    public interface IRefereeRepository
    {
        public Task<IEnumerable<RefereeDTO>> GetAll();
        public Task<RefereeDTO> Get(int refereeId);
        public Task<RefereeDTO> Create(RefereeDTO refereeDTO);
        public Task<RefereeDTO> Update(RefereeDTO refereeDTO, int refereeId);
        public Task<int> Delete(int refereeId);
    }
}
