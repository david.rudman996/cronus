﻿using Models.MatchData;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Business.Repository.IRepository
{
    public interface IMatchBasketballRepository
    {
        public Task<IEnumerable<MatchBasketballDTO>> GetAll();
        public Task<MatchBasketballDTO> Get(int MatchBasketballId);
        public Task<MatchBasketballDTO> GetFromMatch(int matchCoreId);
        public Task<MatchBasketballDTO> Create(MatchBasketballDTO MatchBasketballDTO);
        public Task<MatchBasketballDTO> Update(MatchBasketballDTO MatchBasketballDTO, int MatchBasketballId);
        public Task<MatchBasketballDTO> UpdateMinutesAndStatus(MatchBasketballDTO matchBasketballDTO, int matchBasketballId);
        public Task<int> Delete(int MatchBasketballId);
    }
}
