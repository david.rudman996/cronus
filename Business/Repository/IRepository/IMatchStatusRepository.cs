﻿using Models.MatchData;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Business.Repository.IRepository
{
    public interface IMatchStatusRepository
    {
        public Task<MatchStatusDTO> Get(int matchStatusId);
    }
}
