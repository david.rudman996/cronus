﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Business.Repository.IRepository;
using DataAccess.Data;
using DataAccess.Data.CompetitionData;
using DataAccess.Data.MatchData;
using Microsoft.EntityFrameworkCore;
using Models.CompetitionData;
using Models.MatchData;


namespace Business.Repository
{
    public class CompetitionTeamRepository : ICompetitionTeamRepository
    {
        private readonly ApplicationDbContext _db;
        private readonly ApplicationDbContext _dbSecond;

        private readonly IMapper _mapper;

        public CompetitionTeamRepository(ApplicationDbContext db, IMapper mapper)
        {
            _mapper = mapper;
            _db = db;
            _dbSecond = db;
        }


        public async Task<IEnumerable<CompetitionTeamDTO>> GetAllFromCompetition(int competitionId)
        {
            try
            {
                IEnumerable<CompetitionTeamDTO> competitionTeams =
                _mapper.Map<IEnumerable<CompetitionTeam>, IEnumerable<CompetitionTeamDTO>>(
                    _db.CompetitionTeams
                    .Include(x => x.Team)
                    .Include(x => x.Competition)
                    .Where(x => x.Competition.Id == competitionId));
                return competitionTeams;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public async Task<CompetitionTeamDTO> GetCompTeamFromTeam(int teamId)
        {
            try
            {
                CompetitionTeamDTO competitionTeam = _mapper.Map<CompetitionTeam, CompetitionTeamDTO>(
                    await _db.CompetitionTeams.SingleOrDefaultAsync(x => x.TeamId == teamId));
                return competitionTeam;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<CompetitionTeamDTO> Create(CompetitionTeamDTO competitionTeamDTO)
        {
            CompetitionTeam competitionTeam = _mapper.Map<CompetitionTeamDTO, CompetitionTeam>(competitionTeamDTO);
            var addedCompetitions = await _db.CompetitionTeams.AddAsync(competitionTeam);
            await _db.SaveChangesAsync();
            return _mapper.Map<CompetitionTeam, CompetitionTeamDTO>(addedCompetitions.Entity);
        }

        public async Task<CompetitionTeamDTO> Update(CompetitionTeamDTO competitionTeamDTO, int competitionTeamId)
        {
            try
            {
                if (competitionTeamId == competitionTeamDTO.Id)
                {
                    CompetitionTeam competitionDetails = await _db.CompetitionTeams.FindAsync(competitionTeamId);
                    CompetitionTeam competition = _mapper.Map<CompetitionTeamDTO, CompetitionTeam>(competitionTeamDTO, competitionDetails);
                    var updatedCompetitionTeam = _db.CompetitionTeams.Update(competition);
                    await _db.SaveChangesAsync();
                    return _mapper.Map<CompetitionTeam, CompetitionTeamDTO>(updatedCompetitionTeam.Entity);
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<int> SetAllValuesToZero(int competitionId) // method to set all table values to zero so we can recalculate it again
        {
            IEnumerable<CompetitionTeamDTO> competitionTeams = await GetAllFromCompetition(competitionId);
            foreach(var team in competitionTeams)
            {
                team.Draw = 0;
                team.GoalsScored = 0;
                team.GoalsConceded = 0;
                team.Lost = 0;
                team.Played = 0;
                team.Points = 0;
                team.Rank = 0;
                team.Won = 0;
                await Update(team, team.Id);
            }
            return 1;
        }
        public async Task<int> Recalculate(int competitionId, string sportName)
        {
            try
            {
                IEnumerable<MatchCoreDTO> matchesFromCompetition = _mapper.Map<IEnumerable<MatchCore>, IEnumerable<MatchCoreDTO>>(
                    _db.Matches
                    .Include(x => x.MatchStatus)
                    .Include(x => x.TeamHome)
                    .Include(x => x.TeamAway)
                    .Include(x => x.Competition)
                    .Include(x => x.Round)
                    .Where(x => x.CompetitionId == competitionId));
                IEnumerable<CompetitionTeamDTO> competitionTeams = await GetAllFromCompetition(competitionId);

                await SetAllValuesToZero(competitionId); //we have to set all values to "0" before calculating it again

                if (sportName.Equals("Futsal"))
                {
                    foreach(var match in matchesFromCompetition)
                    {

                        CompetitionTeamDTO homeTeam = await GetCompTeamFromTeam(match.TeamHomeId);

                        CompetitionTeamDTO awayTeam = _mapper.Map<CompetitionTeam, CompetitionTeamDTO>(
                                                         await _dbSecond.CompetitionTeams.SingleOrDefaultAsync(x => x.TeamId == match.TeamAwayId));

                        if (!match.MatchStatus.Status.Equals("Scheduled")) // we look at matches that have finished or are currently live
                        {
                            if (match.TeamHomeScore > match.TeamAwayScore) // if home team is winning or won
                            {
                                //homeTeam details
                                homeTeam.GoalsScored += match.TeamHomeScore;
                                homeTeam.GoalsConceded += match.TeamAwayScore;
                                homeTeam.Played++;
                                homeTeam.Points += 3;
                                homeTeam.Won++;

                                //awayTeam details
                                awayTeam.GoalsScored += match.TeamAwayScore;
                                awayTeam.GoalsConceded += match.TeamHomeScore;
                                awayTeam.Played++;
                                awayTeam.Lost++;

                                var result1 = await Update(homeTeam, homeTeam.Id);
                                var result2 = await Update(awayTeam, awayTeam.Id);
                            }
                            else if (match.TeamHomeScore < match.TeamAwayScore) // if away team is winning or won
                            {
                                awayTeam.GoalsScored += match.TeamAwayScore;
                                awayTeam.GoalsConceded += match.TeamHomeScore;
                                awayTeam.Played++;
                                awayTeam.Points += 3;
                                awayTeam.Won++;

                                homeTeam.GoalsScored += match.TeamHomeScore;
                                homeTeam.GoalsConceded += match.TeamAwayScore;
                                homeTeam.Played++;
                                homeTeam.Lost++;

                                var result1 = await Update(homeTeam, homeTeam.Id);
                                var result2 = await Update(awayTeam, awayTeam.Id);

                            }
                            else if (match.TeamHomeScore == match.TeamAwayScore) // if draw
                            {
                                homeTeam.GoalsScored += match.TeamHomeScore;
                                homeTeam.GoalsConceded += match.TeamAwayScore;
                                homeTeam.Played++;
                                homeTeam.Draw++;
                                homeTeam.Points ++;

                                awayTeam.GoalsScored += match.TeamHomeScore;
                                awayTeam.GoalsConceded += match.TeamAwayScore;
                                awayTeam.Played++;
                                awayTeam.Draw++;
                                awayTeam.Points++;

                                var result1 = await Update(homeTeam, homeTeam.Id);
                                var result2 = await Update(awayTeam, awayTeam.Id);
                            }
                        }
                    }
                }
                else if (sportName.Equals("Basketball"))
                {
                    foreach (var match in matchesFromCompetition)
                    {
                        CompetitionTeamDTO homeTeam = await GetCompTeamFromTeam(match.TeamHomeId);

                        CompetitionTeamDTO awayTeam = _mapper.Map<CompetitionTeam, CompetitionTeamDTO>(
                                                         await _dbSecond.CompetitionTeams.SingleOrDefaultAsync(x => x.TeamId == match.TeamAwayId));

                        if (!match.MatchStatus.Status.Equals("Scheduled")) // we look at matches that have finished or are currently live
                        {
                            if (match.TeamHomeScore > match.TeamAwayScore) // if home team is winning or won
                            {
                                //homeTeam details
                                homeTeam.GoalsScored += match.TeamHomeScore;
                                homeTeam.GoalsConceded += match.TeamAwayScore;
                                homeTeam.Played++;
                                homeTeam.Points += 2;
                                homeTeam.Won++;

                                //awayTeam details
                                awayTeam.GoalsScored += match.TeamAwayScore;
                                awayTeam.GoalsConceded += match.TeamHomeScore;
                                awayTeam.Played++;
                                awayTeam.Lost++;
                                awayTeam.Points ++;

                                var result1 = await Update(homeTeam, homeTeam.Id);
                                var result2 = await Update(awayTeam, awayTeam.Id);
                            }
                            if (match.TeamHomeScore < match.TeamAwayScore) // if away team is winning or won
                            {
                                awayTeam.GoalsScored += match.TeamAwayScore;
                                awayTeam.GoalsConceded += match.TeamHomeScore;
                                awayTeam.Played++;
                                awayTeam.Points += 2;
                                awayTeam.Won++;

                                homeTeam.GoalsScored += match.TeamHomeScore;
                                homeTeam.GoalsConceded += match.TeamAwayScore;
                                homeTeam.Played++;
                                homeTeam.Lost++;
                                awayTeam.Points++;

                                var result1 = await Update(homeTeam, homeTeam.Id);
                                var result2 = await Update(awayTeam, awayTeam.Id);

                            }
                            if (match.TeamHomeScore == match.TeamAwayScore) // if draw
                            {
                                homeTeam.GoalsScored += match.TeamHomeScore;
                                homeTeam.GoalsConceded += match.TeamAwayScore;
                                homeTeam.Played++;
                                homeTeam.Draw++;
                                homeTeam.Points++;

                                awayTeam.GoalsScored += match.TeamAwayScore;
                                awayTeam.GoalsConceded += match.TeamHomeScore;
                                awayTeam.Played++;
                                awayTeam.Draw++;
                                awayTeam.Points++;

                                var result1 = await Update(homeTeam, homeTeam.Id);
                                var result2 = await Update(awayTeam, awayTeam.Id);
                            }
                        }
                    }
                }
                return 1;
            }
            catch (Exception){ return 1; }
            
        }

        public async Task<int> Delete(int competitionTeamId)
        {
            var competitionDetails = await _db.CompetitionTeams.FindAsync(competitionTeamId);
            if (competitionDetails != null)
            {
                _db.CompetitionTeams.Remove(competitionDetails);
                return await _db.SaveChangesAsync();
            }
            return 0;
        }


    }
}
