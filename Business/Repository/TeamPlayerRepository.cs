﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Business.Repository.IRepository;
using DataAccess.Data;
using DataAccess.Data.TeamData;
using Microsoft.EntityFrameworkCore;
using Models.PlayerData;
using Models.TeamData;

namespace Business.Repository
{
    public class TeamPlayerRepository : ITeamPlayerRepository
    {
        private readonly ApplicationDbContext _db;
        private readonly IMapper _mapper;
        public TeamPlayerRepository(ApplicationDbContext db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
        }
        public async Task<IEnumerable<TeamPlayerDTO>> GetAll()
        {
            try
            {
                IEnumerable<TeamPlayerDTO> teamPlayers = _mapper.Map<IEnumerable<TeamPlayer>, IEnumerable<TeamPlayerDTO>>(
                    _db.TeamPlayers
                    .Include(x => x.Player)
                    .Include(x => x.TeamSeason));
                return teamPlayers;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public async Task<IEnumerable<TeamPlayerDTO>> GetAllFromTeamSeason(int teamSeasonId)
        {
            try
            {
                IEnumerable<TeamPlayerDTO> teamPlayers = _mapper.Map<IEnumerable<TeamPlayer>, IEnumerable<TeamPlayerDTO>>(
                    _db.TeamPlayers
                    .Include(x => x.Player)
                    .Include(x => x.TeamSeason)
                    .Where(x => x.TeamSeasonId == teamSeasonId));
                return teamPlayers;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        

        public Task<TeamPlayerDTO> Get(int teamPlayerId)
        {
            throw new NotImplementedException();
        }

        public async Task Create(IEnumerable<PlayerDTO> playerDTOs, int teamSeasonId)
        {
            List<TeamPlayerDTO> teamPlayers = new List<TeamPlayerDTO>();
            foreach(var player in playerDTOs)
            {
                TeamPlayerDTO tempTeamPlayer = new TeamPlayerDTO
                {
                    TeamSeasonId = teamSeasonId,
                    PlayerId = player.Id
                };
                teamPlayers.Add(tempTeamPlayer);
                TeamPlayer teamPlayerToAdd = _mapper.Map<TeamPlayerDTO, TeamPlayer>(tempTeamPlayer);
                
            }
            IEnumerable<TeamPlayer> addedTeamPlayers = _mapper.Map<IEnumerable<TeamPlayerDTO>, IEnumerable<TeamPlayer>>(teamPlayers);
            await _db.TeamPlayers.AddRangeAsync(addedTeamPlayers);
            await _db.SaveChangesAsync();
        }

        public async Task<int> Delete(int teamPlayerId)
        {
            var teamPlayerDetails = await _db.TeamPlayers.FindAsync(teamPlayerId);
            if(teamPlayerDetails != null)
            {
                _db.TeamPlayers.Remove(teamPlayerDetails);
                return await _db.SaveChangesAsync();
            }
            return 0;
        }

    }
}
