﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Business.Repository.IRepository;
using DataAccess.Data;
using DataAccess.Data.MatchData;
using Microsoft.EntityFrameworkCore;
using Models.MatchData;

namespace Business.Repository
{
    public class MatchBasketballRepository : IMatchBasketballRepository
    {
        private readonly ApplicationDbContext _db;

        private readonly IMapper _mapper;

        public MatchBasketballRepository(ApplicationDbContext db, IMapper mapper)
        {
            _mapper = mapper;
            _db = db;
        }


        public async Task<IEnumerable<MatchBasketballDTO>> GetAll()
        {
            try
            {
                IEnumerable<MatchBasketballDTO> matchBasketballs =
                _mapper.Map<IEnumerable<MatchBasketball>, IEnumerable<MatchBasketballDTO>>(
                    _db.MatchBasketballs
                    .Include(x => x.Match)
                    .Include(x => x.Match).ThenInclude(x => x.TeamHome)
                    .Include(x => x.Match).ThenInclude(x => x.TeamAway)
                    .Include(x => x.Match).ThenInclude(x => x.MatchStatus)
                    .Include(x => x.Match)
                    .ThenInclude(x => x.Round)
                    .ThenInclude(x => x.Competition)
                    .ThenInclude(x => x.CompetitionParent)
                    .ThenInclude(x => x.Season));
                return matchBasketballs;
            }
            catch (Exception)
            {
                return null;
            }

        }
        public async Task<MatchBasketballDTO> Get(int matchBasketballId)
        {
            try
            {
                MatchBasketballDTO matchBasketball = _mapper.Map<MatchBasketball, MatchBasketballDTO>(
                await _db.MatchBasketballs
                        .Include(x => x.Match)
                        .ThenInclude(x => x.Competition)
                        .ThenInclude(x => x.CompetitionParent)
                        .ThenInclude(x => x.Season)
                        .SingleOrDefaultAsync(x => x.Id == matchBasketballId));

                if (matchBasketball != null)
                {
                    return matchBasketball;
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<MatchBasketballDTO> GetFromMatch(int matchCoreId)
        {
            try
            {
                MatchBasketballDTO matchBasketball = _mapper.Map<MatchBasketball, MatchBasketballDTO>(
                await _db.MatchBasketballs
                        .Include(x => x.Match)
                        .ThenInclude(x => x.Competition)
                        .ThenInclude(x => x.CompetitionParent)
                        .ThenInclude(x => x.Season)
                        .SingleOrDefaultAsync(x => x.Match.Id == matchCoreId));

                if (matchBasketball != null)
                {
                    return matchBasketball;
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<MatchBasketballDTO> Create(MatchBasketballDTO matchBasketballDTO)
        {
            MatchBasketball matchBasketball = _mapper.Map<MatchBasketballDTO, MatchBasketball>(matchBasketballDTO);
            var addedMatches = await _db.MatchBasketballs.AddAsync(matchBasketball);
            await _db.SaveChangesAsync();
            return _mapper.Map<MatchBasketball, MatchBasketballDTO>(addedMatches.Entity);
        }

        public async Task<MatchBasketballDTO> Update(MatchBasketballDTO matchBasketballDTO, int matchBasketballId)
        {
            try
            {
                if (matchBasketballId == matchBasketballDTO.Id)
                {
                    MatchBasketball matchBasketballDetails = await _db.MatchBasketballs.FindAsync(matchBasketballId);
                    MatchBasketball matchBasketball = _mapper.Map<MatchBasketballDTO, MatchBasketball>(matchBasketballDTO, matchBasketballDetails);
                    var updatedMatchBasketball = _db.MatchBasketballs.Update(matchBasketball);
                    await _db.SaveChangesAsync();
                    return _mapper.Map<MatchBasketball, MatchBasketballDTO>(updatedMatchBasketball.Entity);
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<MatchBasketballDTO> UpdateMinutesAndStatus(MatchBasketballDTO matchBasketballDTO, int matchBasketballId)
        {
            try
            {
                if (matchBasketballId == matchBasketballDTO.Id)
                {
                    MatchBasketball matchBasketballDetails = await _db.MatchBasketballs.FindAsync(matchBasketballId);
                    MatchBasketball matchBasketball = _mapper.Map<MatchBasketballDTO, MatchBasketball>(matchBasketballDTO, matchBasketballDetails);
                    var updatedMatchBasketball = _db.MatchBasketballs.Update(matchBasketball);
                    await _db.SaveChangesAsync();
                    return _mapper.Map<MatchBasketball, MatchBasketballDTO>(updatedMatchBasketball.Entity);
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                return null;
            }
        }


        public async Task<int> Delete(int matchBasketballId)
        {
            var matchBasketballDetails = await _db.MatchBasketballs.FindAsync(matchBasketballId);
            if (matchBasketballDetails != null)
            {
                _db.MatchBasketballs.Remove(matchBasketballDetails);
                return await _db.SaveChangesAsync();
            }
            return 0;
        }
    }
}
