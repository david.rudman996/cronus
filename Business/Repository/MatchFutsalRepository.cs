﻿using System;
using System.Collections.Generic;

using System.Threading.Tasks;
using AutoMapper;
using Business.Repository.IRepository;
using DataAccess.Data;
using DataAccess.Data.MatchData;
using Microsoft.EntityFrameworkCore;
using Models.MatchData;

namespace Business.Repository
{
    public class MatchFutsalRepository : IMatchFutsalRepository
    {
        private readonly ApplicationDbContext _db;

        private readonly IMapper _mapper;

        public MatchFutsalRepository(ApplicationDbContext db, IMapper mapper)
        {
            _mapper = mapper;
            _db = db;
        }
        
        
        public async Task<IEnumerable<MatchFutsalDTO>> GetAll()
        {

            try
            {
                IEnumerable<MatchFutsalDTO> matchFutsals =
                _mapper.Map<IEnumerable<MatchFutsal>, IEnumerable<MatchFutsalDTO>>(
                    _db.MatchFutsals
                    .Include(x => x.Match)
                    .Include(x => x.Match).ThenInclude(x => x.TeamHome)
                    .Include(x => x.Match).ThenInclude(x => x.TeamAway)
                    .Include(x => x.Match).ThenInclude(x => x.MatchStatus)
                    .Include(x => x.Match)
                    .ThenInclude(x => x.Round)
                    .ThenInclude(x => x.Competition)
                    .ThenInclude(x => x.CompetitionParent)
                    .ThenInclude(x => x.Season));
                return matchFutsals;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public async Task<MatchFutsalDTO> GetFromMatch(int matchCoreId)
        {
            try
            {
                MatchFutsalDTO matchFutsal = _mapper.Map<MatchFutsal, MatchFutsalDTO>(
                await _db.MatchFutsals
                        .Include(x => x.Match)
                        .Include(x => x.Match).ThenInclude(x => x.TeamHome).ThenInclude(x => x.Sport)
                        .Include(x => x.Match).ThenInclude(x => x.TeamAway)
                        .Include(x => x.Match).ThenInclude(x => x.MatchStatus)
                        .Include(x => x.Match).ThenInclude(x => x.Round)
                        .Include(x => x.Match).ThenInclude(x => x.Competition).ThenInclude(x => x.CompetitionParent).ThenInclude(x => x.Season)
                        .SingleOrDefaultAsync(x => x.Match.Id== matchCoreId));

                if (matchFutsal != null)
                {
                    return matchFutsal;
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }
        
        public async Task<MatchFutsalDTO> Get(int matchId)
        {
            try
            {
                MatchFutsalDTO matchFutsal = _mapper.Map<MatchFutsal, MatchFutsalDTO>(
                await _db.MatchFutsals
                        .Include(x => x.Match).ThenInclude(x => x.TeamHome)
                        .Include(x => x.Match).ThenInclude(x => x.TeamAway)
                        .Include(x => x.Match).ThenInclude(x => x.Competition).ThenInclude(x => x.CompetitionParent)
                        .Include(x => x.Match).ThenInclude(x => x.Round)
                        .Include(x => x.Match).ThenInclude(x => x.MatchStatus)
                        .SingleOrDefaultAsync(x => x.Id == matchId));
                if (matchFutsal != null)
                {
                    return matchFutsal;
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }

        }

        public async Task<MatchFutsalDTO> Create(MatchFutsalDTO matchFutsalDTO)
        {
            MatchFutsal matchFutsal = _mapper.Map<MatchFutsalDTO, MatchFutsal>(matchFutsalDTO);
            var addedMatches = await _db.MatchFutsals.AddAsync(matchFutsal);
            await _db.SaveChangesAsync();
            return _mapper.Map<MatchFutsal, MatchFutsalDTO>(addedMatches.Entity);
        }

        public async Task<MatchFutsalDTO> Update(MatchFutsalDTO matchFutsalDTO, int matchFutsalId)
        {
            try
            {
                if (matchFutsalId == matchFutsalDTO.Id)
                {
                    MatchFutsal matchFutsalDetails = await _db.MatchFutsals.FindAsync(matchFutsalId);
                    MatchFutsal matchFutsal = _mapper.Map<MatchFutsalDTO, MatchFutsal>(matchFutsalDTO, matchFutsalDetails);
                    var updatedMatchFutsal = _db.MatchFutsals.Update(matchFutsal);
                    await _db.SaveChangesAsync();
                    return _mapper.Map<MatchFutsal, MatchFutsalDTO>(updatedMatchFutsal.Entity);
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<MatchFutsalDTO> UpdateMinutesAndStatus(MatchFutsalDTO matchFutsalDTO, int matchFutsalId)
        {
            try
            {
                if (matchFutsalId == matchFutsalDTO.Id)
                {
                    MatchFutsal matchFutsalDetails = await _db.MatchFutsals.FindAsync(matchFutsalId);
                    MatchFutsal matchFutsal = _mapper.Map<MatchFutsalDTO, MatchFutsal>(matchFutsalDTO, matchFutsalDetails);
                    var updatedMatchFutsal = _db.MatchFutsals.Update(matchFutsal);
                    await _db.SaveChangesAsync();
                    return _mapper.Map<MatchFutsal, MatchFutsalDTO>(updatedMatchFutsal.Entity);
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<int> Delete(int matchFutsalId)
        {
            var matchFutsalDetails = await _db.MatchFutsals.FindAsync(matchFutsalId);
            if (matchFutsalDetails != null)
            {
                _db.MatchFutsals.Remove(matchFutsalDetails);
                return await _db.SaveChangesAsync();
            }
            return 0;
        }
    }
}
